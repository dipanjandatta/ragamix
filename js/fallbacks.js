$(document).ready(function(){        
  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/msgcontent", type: "GET",
   beforeSend: function() {
      $("#msgcnt").html();
   },
   success: function(data) {
      $("#msgcnt").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getmessageusers", type: "GET",
   beforeSend: function() {
      $("#leftcolumn").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#leftcolumn").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/gettrendingvideoslist", type: "GET",
   beforeSend: function() {
      $("#listers").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#listers").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/gettrendingvideosband", type: "GET",
   beforeSend: function() {
      $("#listersbandvids").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#listersbandvids").html(data);
   }
});


  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/gettrendingvideossingers", type: "GET",
   beforeSend: function() {
      $("#listerssingersvids").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#listerssingersvids").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/gettrendingaudiolist", type: "GET",
   beforeSend: function() {
      $("#listersaudio").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#listersaudio").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/gettrendingaudioband", type: "GET",
   beforeSend: function() {
      $("#listersbandaudio").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#listersbandaudio").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/gettrendingaudiosingers", type: "GET",
   beforeSend: function() {
      $("#listerssingersaudio").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#listerssingersaudio").html(data);
   }
});


  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getmusicianinarea", type: "GET",
   beforeSend: function() {
      $("#listermusicianarea").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#listermusicianarea").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getbandsarea", type: "GET",
   beforeSend: function() {
      $("#listerbandarea").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#listerbandarea").html(data);
   }
});


  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getsingersarea", type: "GET",
   beforeSend: function() {
      $("#listersingersarea").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#listersingersarea").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getscheduledevents", type: "GET",
   beforeSend: function() {
      $("#scev").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#scev").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getglobalevents", type: "GET",
   beforeSend: function() {
      $("#ge").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#ge").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getreviews", type: "GET",
   beforeSend: function() {
      $("#gr").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#gr").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getspotlightnewsbits", type: "GET",
   beforeSend: function() {
      $("#spnb").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#spnb").html(data);
   }
});



  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getfeaturedmusician", type: "GET",
   beforeSend: function() {
      $("#featuredmusician").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#featuredmusician").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getfeaturedsingers", type: "GET",
   beforeSend: function() {
      $("#featuredsingers").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#featuredsingers").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getfeaturedbands", type: "GET",
   beforeSend: function() {
      $("#featuredbands").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#featuredbands").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getfeaturedevents", type: "GET",
   beforeSend: function() {
      $("#featuredevents").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#featuredevents").html(data);
   }
});


  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getglobaleventshomepage", type: "GET",
   beforeSend: function() {
      $("#homepageeventslist").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#homepageeventslist").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getspotlightnewsbitshomepage", type: "GET",
   beforeSend: function() {
      $("#newsbitshomepage").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#newsbitshomepage").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getadshomepage", type: "GET",
   beforeSend: function() {
      $("#getlatestadshomepage").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#getlatestadshomepage").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/gethomepagereviews", type: "GET",
   beforeSend: function() {
      $("#getreviewshomepage").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#getreviewshomepage").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getfeaturedads", type: "GET",
   beforeSend: function() {
      $("#featuredads").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#featuredads").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getwantedads", type: "GET",
   beforeSend: function() {
      $("#adswanted").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#adswanted").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getbuysellads", type: "GET",
   beforeSend: function() {
      $("#adsbuysell").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#adsbuysell").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getadsoffer", type: "GET",
   beforeSend: function() {
      $("#adsoffer").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#adsoffer").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/gettopspots", type: "GET",
   beforeSend: function() {
      $("#topspots").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#topspots").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/getfeaturedprofilehome", type: "GET",
   beforeSend: function() {
      $("#fphome").html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
   },
   success: function(data) {
      $("#fphome").html(data);
   }
});

  $.ajax({
   url: window.location.origin+"/ragamix/ajaxcontent/searchcontainer", type: "GET",
   success: function(data) {
      $("#searchContainer").html(data);
   }
});


      
        $(".fm").click(function() 
        {
                var filmusi = $('#filtermusician').val();
                $.ajax({
                type: "POST",
                url: window.location.origin+"/ragamix/ajaxcontent/getmusicianinarea",
                data: 'city='+filmusi,
                cache: false,
                beforeSend: function(){
                    $('#listermusicianarea').html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
                },
                success: function(html)
                    {
                        $("#listermusicianarea").html(html);
                    }
                });
        return false; 
        });
        
        $(".fbd").click(function() 
        {
                var filband = $('#filterband').val();
                $.ajax({
                type: "POST",
                url: window.location.origin+"/ragamix/ajaxcontent/getbandsarea",
                data: 'city='+filband,
                cache: false,
                beforeSend: function(){
                    $('#listerbandarea').html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
                },
                success: function(html)
                    {
                        $("#listerbandarea").html(html);
                    }
                });
        return false; 
        });
       
        $(".course").keyup(function() 
        {
            var min_length = 0;
            var coursebox = $(this).val();
            var dataString = 'searchword='+ coursebox;
            if(coursebox.length > min_length)
            {
                $.ajax({
                type: "POST",
                url: window.location.origin+"/ragamix/ajaxcontent/getuserslist",
                data: dataString,
                cache: false,
                beforeSend: function(){
                    $('#displays').html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' />");
                },
                success: function(html)
                    {
                        $("#displays").html(html).show();
                    }
                });
            }
            else
            {
                $("#displays").hide();
            }return false; 
        });

$(".update_button").click(function() {

var boxval = $("#content").val();
var from_id = $("#contentsender").val();
var to_id = $("#courseboxid").val();
var dataString = 'content='+ boxval +'&contentsender='+ from_id +'&courseboxid='+ to_id;

if(boxval=='')
{
alert("Please Enter Some Text");
}
else
{
$("#flash").show();
$("#flash").fadeIn(400).html("<img src='"+ window.location.origin + "/ragamix/images/loading.gif' align='absmiddle' /> <span class='loading'>Sending...</span>");

$.ajax({
type: "POST",
url: window.location.origin+"/ragamix/ajaxcontent/sendmessage",
data: dataString,
cache: false,
success: function(html){
$("ol#update").prepend(html);
$("ol#update li:first").slideDown("slow");
document.getElementById('content').value='';
document.getElementById('content').focus();
$("#flash").hide();
}
});
} return false;
});


    $("#filter").keyup(function(){
        var filter = $(this).val(), count = 0;
        $(".commentList").each(function(){
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
            } else {
                $(this).show();
                count++;
            }
        });
        var numberItems = count;
        $("#filter-count").text(count+ " Filtered Found");
    });

    $("#hideHomeOne").click(function(){
        $(".nbts").toggle();
    });
    $("#hideHomeTwo").click(function(){
        $(".fps").toggle();
    });
    $("#hideHomeThree").click(function(){
        $(".rhp").toggle();
    });
    
    	$('.cd-btn').on('click', function(event){
		event.preventDefault();
		$('.cd-panel').addClass('is-visible');
	});
	$('.cd-panel').on('click', function(event){
		if( $(event.target).is('.cd-panel') || $(event.target).is('.cd-panel-close') ) { 
			$('.cd-panel').removeClass('is-visible');
			event.preventDefault();
		}
	});
    
    
});

$('#myCarousel').carousel({
  interval: 3000,
  cycle: true
}); 


 
