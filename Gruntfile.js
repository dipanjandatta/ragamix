module.exports = function(grunt) {

    grunt.initConfig({
        concat: {
            dist: {
                src: ['css/*.css'],
                dest: 'css/compiled.css'
                },
            php: {

                src: ['application/views/layouts/main.php'],

                dest: 'application/views/layouts/concat.php'

            	}
            },
	/*uglify: {
	      build: {
		files: [{
		    expand: true,
		    src: ['js/jquery.js','js/bootstrap.js','js/growl.js','js/masonry.js','js/mb.js'],
		    dest: 'build/scripts'
		}]
	      }
	    },*/
        uglify: {
            dist: {
                src: ['js/jquery.js','js/bootstrap.js','js/growl.js','js/mb.js','js/openpipe.js','js/ays.js','js/tabs.js','js/validation.js','js/calender.js','js/app.js','js/fallbacks.js','js/typeahead.js','js/slideshow.js','js/bootstrap-dialog.js'],
                dest: 'js/main.min.js'
                }
            },
        cssmin: {
            dist: {
                src: 'css/compiled.css',
                dest: 'css/compiled.min.css'
                }
            },

htmlmin: {

    dist: {
        options: {
            removeComments: true,
            collapseWhitespace: true
        },

        tasks: ['clean:php'],
        files: {
            'application/views/layouts/main_dist.php': 'application/views/layouts/concat.php',
        }
    }
}

        });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.registerTask('build', ['concat', 'uglify', 'cssmin', 'htmlmin']);
};
