<?php $this->load->view('layouts/admin/main'); ?>
<div class="container">
    <div class="pagingholder">
        <div class="filterOps">
            <a href="<?php echo base_url(); ?>dashboard/adlist?q=2&next=1&clicks=0&page_number=0" style="padding: 10px 50px; border-right: 1px solid #d8d8d8;float: left;">All</a>
            <a href="<?php echo base_url(); ?>dashboard/adlist?q=1&&next=1&clicks=0&page_number=0" style="padding: 10px 50px; border-right: 1px solid #d8d8d8;float: left;">Approved</a>
            <a href="<?php echo base_url(); ?>dashboard/adlist?q=0&&next=1&clicks=0&page_number=0" style="padding: 10px 50px; border-right: 1px solid #d8d8d8;float: left;">Unapproved</a>
        </div>
        <?php if($page_number > 0){ ?>
            <a href="<?php echo base_url(); ?>dashboard/adlist?q=<?php echo $q; ?>&next=<?php echo $next; ?>&clicks=<?php echo ($clicks-1); ?>&page_number=<?php echo ($page_number-1); ?>&prev=y">
                <i class="fa fa-chevron-left pageicon"></i>
            </a>
        <?php } ?>
        <a href="<?php echo base_url(); ?>dashboard/adlist?q=<?php echo $q; ?>&next=<?php echo $next; ?>&clicks=<?php echo $clicks; ?>&page_number=<?php echo $page_number; ?>">
            <i class="fa fa-chevron-right pageicon"></i>
        </a>
    </div>
    <?php
    foreach($getAdsList as $val):
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->doc_youtube_url, $match)) {
            $vid = $match[1];
            $displayedYoutubeVideo = "<iframe width='240' height='165' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
        }
        ?>
        <a href="<?php echo base_url(); ?>dashboard/ads/<?php echo $val->ad_id; ?>">
            <div class="col-md-2-3 noPad stillBord">
                <div class="demo-section k-content stillHeight">
                    <div class="coverClass blurim">
                        <?php if($val->pic_url != '') { ?>
                            <img src="<?php echo $val->pic_url;?>" class="newImg" />
                        <?php } else {
                            echo $displayedYoutubeVideo;
                        }
                        ?>
                    </div>
                    <div class="title grey">
                        <?php echo $val->ads_title; ?>
                    </div>
                    <div class="stnComm greyblack">
                        <?php
                        if(strlen($val->ads_desc) > 100) {
                            // truncate string
                            $stringCut = substr($val->ad_desc, 0, 100);

                            // make sure it ends in a word so assassinate doesn't become ass...
                            $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
                        }
                        else {
                            $string = $val->ads_desc;
                        }
                        echo $string;
                        ?>
                    </div>
                </div>
            </div>
        </a>
    <?php endforeach; ?>
</div>