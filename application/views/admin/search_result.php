
<?php $this->load->view('layouts/admin/main'); ?>


<div class="fixedhead">
  <div class="container">
    <div class="page-header clearfix">
      <div class="col-md-3">
        <h2>Your Search Results</h2></div>
      <div class="col-md-7">
        <div  style="margin-top: 4px;" class="dataTables_wrapper">
          <!--<div class="dataTables_paginate paging_simple_numbers" id="example0_paginate"><a class="paginate_button previous disabled" aria-controls="example0" data-dt-idx="0" tabindex="0" id="example0_previous">Previous</a><span><a class="paginate_button current" aria-controls="example0" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="example0" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="example0" data-dt-idx="3" tabindex="0">3</a><a class="paginate_button " aria-controls="example0" data-dt-idx="4" tabindex="0">4</a></span><a class="paginate_button next" aria-controls="example0" data-dt-idx="5" tabindex="0" id="example0_next">Next</a></div>-->
          <pagination
            ng-model="currentPage"
            total-items="profileInfoList.length"
            max-size="maxSize"
            boundary-links="true">
          </pagination>
        </div>
      </div>
      <div class="col-md-2">
        <div class="right">
          <div class="btn-group viewbtn-group" role="group" >

            <a id="search-view-list" href="" class="btn btn-light"><i class="fa fa-th-list"></i></a>
            <a id="search-view-grid" href="" class="btn btn-light active"><i class="fa fa-th-large"></i></a>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-9 ">
        <div class="searchgrid ">
          <div class="clearfix searchgridoption " >
            <div class="dataTables_info" >Showing {{begin+1}} to {{end}} of {{profileInfoList.length}} entries</div>
            <!--<div class="searchsort"><label>Sort By </label><select class="form-control"><option>Option</option></select></div>-->
          </div>
          <div class="clearfix">
            <ul class="searchlisting clearfix">
              <li class="premium" ng-repeat="profileInfo in filteredprofileInfoList">
                <div class="imageHolder">
                  <a class="imgview" href="" ng-click="viewprofile(profileInfo.rm_id)">
                    <img ng-src="http://cdn.ragamix.com/{{profileInfo.main_pic_path_url}}"/>
                  </a>
                  <div class="searchlisting-desc">
                    <a class="name">
                      {{profileInfo.user_name}}
                    </a>
                    <div class="button">

                      <!--<button type="button" class="btn btn-default btn-icon viewbtn" ng-click="viewprofile(profileInfo.rm_id)"><i class="fa fa-eye">Edit</i></button>
                      <button type="button" class="btn btn-default btn-icon msgbtn" ng-click="deleteprofile(profileInfo.rm_id)"><i class="fa fa-envelope-o">Delete</i></button>
                      <button type="button" class="btn btn-default btn-icon plusbtn" ng-click="blockprofile(profileInfo.rm_id)"><i class="fa fa-plus">Block</i></button>-->
                    </div>
                    <div class="feat">
                      <i class="fa fa-play-circle"></i> {{ profileInfo.usertypename | limitTo: 30 }}{{profileInfo.usertypename.length > 30 ? '...' : ''}}<!--Bass Guitar, Upright bass-->
                    </div>
                    <div class="location">
                      <i class="fa fa-map-marker"></i> {{ profileInfo.location_info | limitTo: 40 }}{{profileInfo.location_info.length > 40 ? '...' : ''}} <!--{{profileInfo.street_1}}, {{profileInfo.city_name}}, {{profileInfo.pin_code}}-->
                      <!--Kendall Park, New Jersey, 08824-->
                    </div>
                    <div class="music">
                      <i class="fa fa-music"></i> {{ profileInfo.genre_name | limitTo: 40 }}{{profileInfo.genre_name.length > 40 ? '...' : ''}}<!--R&B, Jazz, Funk, Hip Hop/Rap-->
                    </div>
                    <div class="active">
                      <!--Active within {{profileInfo.activehrs}} hours-->
                      <button type="button" class="btn btn-default" ng-click="deleteprofile(profileInfo.rm_id)">Delete</button>
                    </div>
                  </div>
                </div>

              </li>
            </ul>
            <!--<ul class="searchlisting clearfix">
            <li class="premium">
                <div class="imageHolder">
                    <a class="imgview" href="#/main/profile">
                        <img  src="images/event1.jpg"/>

                    </a>
                    <div class="searchlisting-desc">
                        <a class="name">
                            Name
                        </a>
                        <div class="button">
                            <button type="button" class="btn btn-default btn-icon viewbtn"><i class="fa fa-eye"></i></button>
                            <button type="button" class="btn btn-default btn-icon msgbtn"><i class="fa fa-envelope-o"></i></button>
                            <button type="button" class="btn btn-default btn-icon plusbtn"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="feat">
                            <i class="fa fa-play-circle"></i> Bass Guitar, Upright bass
                        </div>
                        <div class="location">
                            <i class="fa fa-map-marker"></i> Musician | Kendall Park, New Jersey, 08824
                        </div>
                        <div class="music">
                            <i class="fa fa-music"></i> R&B, Jazz, Funk, Hip Hop/Rap
                        </div>
                        <div class="active">
                            Active within 24 hours
                        </div>
                    </div>
                </div>

            </li>
            <li  class="premium">
                <div class="imageHolder">
                    <a class="imgview" href="#/main/profile">
                        <img  src="images/event1.jpg"/>

                    </a>
                    <div class="searchlisting-desc">
                        <a class="name">
                            Name
                        </a>
                        <div class="button">
                            <button type="button" class="btn btn-default btn-icon viewbtn"><i class="fa fa-eye"></i></button>
                            <button type="button" class="btn btn-default btn-icon msgbtn"><i class="fa fa-envelope-o"></i></button>
                            <button type="button" class="btn btn-default btn-icon plusbtn"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="feat">
                            <i class="fa fa-play-circle"></i> Bass Guitar, Upright bass
                        </div>
                        <div class="location">
                            <i class="fa fa-map-marker"></i> Musician | Kendall Park, New Jersey, 08824
                        </div>
                        <div class="music">
                            <i class="fa fa-music"></i> R&B, Jazz, Funk, Hip Hop/Rap
                        </div>
                        <div class="active">
                            Active within 24 hours
                        </div>
                    </div>
                </div>

            </li>
            <li  class="premium">
                <div class="imageHolder">
                    <a class="imgview" href="#/main/profile">
                        <img  src="images/event1.jpg"/>

                    </a>
                    <div class="searchlisting-desc">
                        <a class="name">
                            Name
                        </a>
                        <div class="button">
                            <button type="button" class="btn btn-default btn-icon viewbtn"><i class="fa fa-eye"></i></button>
                            <button type="button" class="btn btn-default btn-icon msgbtn"><i class="fa fa-envelope-o"></i></button>
                            <button type="button" class="btn btn-default btn-icon plusbtn"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="feat">
                            <i class="fa fa-play-circle"></i> Bass Guitar, Upright bass
                        </div>
                        <div class="location">
                            <i class="fa fa-map-marker"></i> Musician | Kendall Park, New Jersey, 08824
                        </div>
                        <div class="music">
                            <i class="fa fa-music"></i> R&B, Jazz, Funk, Hip Hop/Rap
                        </div>
                        <div class="active">
                            Active within 24 hours
                        </div>
                    </div>
                </div>

            </li>
            <li>
                <div class="imageHolder">
                    <a class="imgview" href="#/main/profile">
                        <img  src="images/event1.jpg"/>

                    </a>
                    <div class="searchlisting-desc">
                        <a class="name">
                            Name
                        </a>
                        <div class="button">
                            <button type="button" class="btn btn-default btn-icon viewbtn"><i class="fa fa-eye"></i></button>
                            <button type="button" class="btn btn-default btn-icon msgbtn"><i class="fa fa-envelope-o"></i></button>
                            <button type="button" class="btn btn-default btn-icon plusbtn"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="feat">
                            <i class="fa fa-play-circle"></i> Bass Guitar, Upright bass
                        </div>
                        <div class="location">
                            <i class="fa fa-map-marker"></i> Musician | Kendall Park, New Jersey, 08824
                        </div>
                        <div class="music">
                            <i class="fa fa-music"></i> R&B, Jazz, Funk, Hip Hop/Rap
                        </div>
                        <div class="active">
                            Active within 24 hours
                        </div>
                    </div>
                </div>

            </li>
            <li>
                <div class="imageHolder">
                    <a class="imgview" href="#/main/profile">
                        <img  src="images/event1.jpg"/>

                    </a>
                    <div class="searchlisting-desc">
                        <a class="name">
                            Name
                        </a>
                        <div class="button">
                            <button type="button" class="btn btn-default btn-icon viewbtn"><i class="fa fa-eye"></i></button>
                            <button type="button" class="btn btn-default btn-icon msgbtn"><i class="fa fa-envelope-o"></i></button>
                            <button type="button" class="btn btn-default btn-icon plusbtn"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="feat">
                            <i class="fa fa-play-circle"></i> Bass Guitar, Upright bass
                        </div>
                        <div class="location">
                            <i class="fa fa-map-marker"></i> Musician | Kendall Park, New Jersey, 08824
                        </div>
                        <div class="music">
                            <i class="fa fa-music"></i> R&B, Jazz, Funk, Hip Hop/Rap
                        </div>
                        <div class="active">
                            Active within 24 hours
                        </div>
                    </div>
                </div>

            </li>
            <li>
                <div class="imageHolder">
                    <a class="imgview" href="#/main/profile">
                        <img  src="images/event1.jpg"/>

                    </a>
                    <div class="searchlisting-desc">
                        <a class="name">
                            Name
                        </a>
                        <div class="button">
                            <button type="button" class="btn btn-default btn-icon viewbtn"><i class="fa fa-eye"></i></button>
                            <button type="button" class="btn btn-default btn-icon msgbtn"><i class="fa fa-envelope-o"></i></button>
                            <button type="button" class="btn btn-default btn-icon plusbtn"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="feat">
                            <i class="fa fa-play-circle"></i> Bass Guitar, Upright bass
                        </div>
                        <div class="location">
                            <i class="fa fa-map-marker"></i> Musician | Kendall Park, New Jersey, 08824
                        </div>
                        <div class="music">
                            <i class="fa fa-music"></i> R&B, Jazz, Funk, Hip Hop/Rap
                        </div>
                        <div class="active">
                            Active within 24 hours
                        </div>
                    </div>
                </div>

            </li>
            <li>
                <div class="imageHolder">
                    <a class="imgview" href="#/main/profile">
                        <img  src="images/event1.jpg"/>

                    </a>
                    <div class="searchlisting-desc">
                        <a  class="name">
                            Name
                        </a>
                        <div class="button">
                            <button type="button" class="btn btn-default btn-icon viewbtn"><i class="fa fa-eye"></i></button>
                            <button type="button" class="btn btn-default btn-icon msgbtn"><i class="fa fa-envelope-o"></i></button>
                            <button type="button" class="btn btn-default btn-icon plusbtn"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="feat">
                            <i class="fa fa-play-circle"></i> Bass Guitar, Upright bass
                        </div>
                        <div class="location">
                            <i class="fa fa-map-marker"></i> Musician | Kendall Park, New Jersey, 08824
                        </div>
                        <div class="music">
                            <i class="fa fa-music"></i> R&B, Jazz, Funk, Hip Hop/Rap
                        </div>
                        <div class="active">
                            Active within 24 hours
                        </div>
                    </div>
                </div>

            </li>
            <li>
                <div class="imageHolder">
                    <a class="imgview" href="#/main/profile">
                        <img  src="images/event1.jpg"/>

                    </a>
                    <div class="searchlisting-desc">
                        <a class="name">
                            Name
                        </a>
                        <div class="button">
                            <button type="button" class="btn btn-default btn-icon viewbtn"><i class="fa fa-eye"></i></button>
                            <button type="button" class="btn btn-default btn-icon msgbtn"><i class="fa fa-envelope-o"></i></button>
                            <button type="button" class="btn btn-default btn-icon plusbtn"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="feat">
                            <i class="fa fa-play-circle"></i> Bass Guitar, Upright bass
                        </div>
                        <div class="location">
                            <i class="fa fa-map-marker"></i> Musician | Kendall Park, New Jersey, 08824
                        </div>
                        <div class="music">
                            <i class="fa fa-music"></i> R&B, Jazz, Funk, Hip Hop/Rap
                        </div>
                        <div class="active">
                            Active within 24 hours
                        </div>
                    </div>
                </div>

            </li>
            <li>
                <div class="imageHolder">
                    <a class="imgview" href="#/main/profile">
                        <img  src="images/event1.jpg"/>

                    </a>
                    <div class="searchlisting-desc">
                        <a class="name">
                            Name
                        </a>
                        <div class="button">
                            <button type="button" class="btn btn-default btn-icon viewbtn"><i class="fa fa-eye"></i></button>
                            <button type="button" class="btn btn-default btn-icon msgbtn"><i class="fa fa-envelope-o"></i></button>
                            <button type="button" class="btn btn-default btn-icon plusbtn"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="feat">
                            <i class="fa fa-play-circle"></i> Bass Guitar, Upright bass
                        </div>
                        <div class="location">
                            <i class="fa fa-map-marker"></i> Musician | Kendall Park, New Jersey, 08824
                        </div>
                        <div class="music">
                            <i class="fa fa-music"></i> R&B, Jazz, Funk, Hip Hop/Rap
                        </div>
                        <div class="active">
                            Active within 24 hours
                        </div>
                    </div>
                </div>

            </li>
            </ul>-->
          </div>
          <div class="dataTables_wrapper">
            <!--<div class="dataTables_paginate paging_simple_numbers" id="example1_paginate"><a class="paginate_button previous disabled" aria-controls="example0" data-dt-idx="0" tabindex="0" id="example0_previous">Previous</a><span><a class="paginate_button current" aria-controls="example0" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="example0" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="example0" data-dt-idx="3" tabindex="0">3</a><a class="paginate_button " aria-controls="example0" data-dt-idx="4" tabindex="0">4</a></span><a class="paginate_button next" aria-controls="example0" data-dt-idx="5" tabindex="0" id="example1_next">Next</a></div>-->
            <pagination
              ng-model="currentPage"
              total-items="profileInfoList.length"
              max-size="maxSize"
              class="dataTables_paginate paging_simple_numbers"
              boundary-links="true">
            </pagination>
          </div>
        </div>
      </div>
      <!--<div class="col-sm-3">

        <div class="single-event searchfilter">
          <h2>Search Filters</h2>
          <div class="form-group">
            <label class="form-label">I am looking for</label>

            <select class="form-control"><option>Option</option></select>
          </div>
          <div class="form-group">
            <label class="form-label">Type Of Industry Listing</label>

            <select class="form-control"><option>Option</option></select>
          </div>
          <div class="form-group">

            <select style="height: 150px" multiple="" class="form-control">
              <option>Option</option>
              <option>Option</option>
              <option>Option</option>
              <option>Option</option>
              <option>Option</option>
              <option>Option</option>
              <option>Option</option>
              <option>Option</option>
            </select>
          </div>-->
        </div>
      </div>
    </div>
  </div>
</section><!--/#buy-ticket-->


<!--<script type="text/javascript" src="js/jquery.js"></script>-->
<!--<script type="text/javascript" src="js/bootstrap.min.js"></script>-->
<!--<script type="text/javascript" src="js/moment.js"></script>-->
<!--<script type="text/javascript" src="js/common.js"></script>-->

