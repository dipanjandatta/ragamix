
<?php $this->load->view('layouts/admin/main'); ?>

<div class="row ng-scope">

    <div class="col-lg-6">
        <srd-widget>
            <div class="widget" >
                <srd-widget-header icon="fa-tasks" title="Servers" class="ng-scope ng-isolate-scope">
                    <div class="widget-header ng-binding" style="font-size: 24px; font-weight: bold;text-align: center">
                        City

                    </div>
                </srd-widget-header>
                <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope" >

                    <div style="border: 1px solid #ffffff; padding: 6%;    margin-left: 40px;">

                        <div ng-hide="loading" class="widget-content" >
                            <div class="table-responsive ng-scope form-horizontal">

                                <div class="form-group form-group-lg othpadding" style="margin-top: 15px; margin-right: 1px;">
                                    <label class="col-sm-4 control-label labelcolor">City Name</label>
                                    <div class="col-sm-8" >
                                        <input class="form-control" type="text">
                                    </div>
                                </div>

                                <div class="form-group form-group-lg othpadding" style="margin-top: 15px; margin-right: 1px;">
                                    <label class="col-sm-4 control-label labelcolor">City Id</label>
                                    <div class="col-sm-8" >
                                        <input class="form-control" type="text">
                                    </div>
                                </div>

                                <div class="pull-right othpadding" style="margin-top: 5px;">
                                    <button class="btn btn-sm btn-info margin7 ng-scope" ng-click="city_save();">Save</button>
                                    <button class="btn btn-sm btn-info margin7 ng-scope" ng-click="clear_data_city();">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </srd-widget-body>
            </div>
        </srd-widget>
    </div>
    <div class="col-lg-6">
        <srd-widget>
            <div class="widget" style="border: 1px solid #ffffff;margin-right: 5%; margin-top: 5%;">
                <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope">
                    <div class="widget-body medium no-padding" style="height: 500px; overflow: scroll" ng-class="classes">
                        <rd-loading ng-show="loading" class="ng-hide">
                            <div class="loading">
                                <div class="double-bounce1"></div>
                                <div class="double-bounce2"></div>
                            </div>
                        </rd-loading>
                        <div ng-hide="loading" class="widget-content" >
                            <div class="table-responsive ng-scope">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th >City</th>
                                        <th >City ID</th>
                                        <th>Select</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($citydata as $val):
                                    ?>

                                    <tr>
                                        <td height="5px"></td>
                                        <td height="5px"></td>
                                        <td class="text-left columnwidth" height="5px">
                                            <input type="checkbox"/>
                                        </td>


                                        <td class="text-left columnwidth" height="5px">
                                            <button  style="margin-top: 10px;">Edit</button>
                                        </td>

                                    </tr>
                                        <?php
                                    endforeach;
                                    ?>

                                    </tbody>
                                </table>
                                <div class="pull-right othpadding" >
                                    <button class="btn btn-sm btn-info margin7 ng-scope" style="margin-top: 10px;margin-right: 10px;">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </srd-widget-body>

            </div>
        </srd-widget>

        <!--<div class="pull-right othpadding" >-->
        <!--<button class="btn btn-sm btn-info margin7 ng-scope" ng-click="delete_user_type()">Delete</button>-->
        <!--</div>-->
    </div>
</div>

<html>
<head>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script language="javascript" type="text/javascript">

        var map;
        var geocoder;
        function InitializeMap() {

            var latlng = new google.maps.LatLng(-34.397, 150.644);
            var myOptions =
            {
                zoom: 8,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true
            };
            map = new google.maps.Map(document.getElementById("map"), myOptions);
        }

        function FindLocaiton() {
            geocoder = new google.maps.Geocoder();
            InitializeMap();

            var address = document.getElementById("addressinput").value;
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });

                }
                else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });

        }


        function Button1_onclick() {
            FindLocaiton();
        }

        window.onload = InitializeMap;

    </script>
</head>
<body>

</body>
</html>