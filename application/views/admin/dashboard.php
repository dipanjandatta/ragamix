
<?php $this->load->view('layouts/admin/main'); ?>

<div class="col-sm-3 col-lg-3">
    <div class="dash-unit">
        <div><dtitle>Activity Console</dtitle></div>
        <hr>
        <!--        <div class="thumbnail">-->
        <!--            <img src="images/face80x80.jpg" alt="Marcel Newman" class="img-circle">-->
        <!--        </div><!-- /thumbnail -->
        <!--        <h1>Marcel Newman</h1>-->
        <!--        <h3>Madrid, Spain</h3>-->
        <ul>
            <li><a href="<?php echo base_url(); ?>dashboard/activity_console"> Data extraction > excel</a></li>
            <li><a>Counters</a></li>
        </ul>

        <br>
        <div class="info-user">
            <span aria-hidden="true" class="li_user fs1"></span>
            <span aria-hidden="true" class="li_settings fs1"></span>
            <span aria-hidden="true" class="li_mail fs1"></span>
            <span aria-hidden="true" class="li_key fs1"></span>
        </div>
    </div>
</div>



<div class="col-sm-3 col-lg-3">
    <div class="dash-unit">
        <div><dtitle>User Priviledge</dtitle></div>
        <hr>
        <!--        <div class="thumbnail">-->
        <!--            <img src="images/face80x80.jpg" alt="Marcel Newman" class="img-circle">-->
        <!--        </div><!-- /thumbnail -->
        <!--        <h1>Marcel Newman</h1>-->
        <!--        <h3>Madrid, Spain</h3>-->

        <ul>
            <li><a href="<?php echo base_url(); ?>dashboard/rolesright">Roles and Right</a></li>
            <li><a href="">Subscription Status</a></li>
        </ul>

        <br>
        <div class="info-user">
            <span aria-hidden="true" class="li_user fs1"></span>
            <span aria-hidden="true" class="li_settings fs1"></span>
            <span aria-hidden="true" class="li_mail fs1"></span>
            <span aria-hidden="true" class="li_key fs1"></span>
        </div>
    </div>
</div>

<div class="col-sm-3 col-lg-3">
    <div class="dash-unit">
        <div><dtitle>Featured Category</dtitle></div>
        <hr>
        <!--        <div class="thumbnail">-->
        <!--            <img src="images/face80x80.jpg" alt="Marcel Newman" class="img-circle">-->
        <!--        </div><!-- /thumbnail -->
        <!--        <h1>Marcel Newman</h1>-->
        <!--        <h3>Madrid, Spain</h3>-->

        <ul>
            <li><a href="<?php echo base_url(); ?>batch/featuredProfiles">Featured Profiles</a></li>
            <li><a href="<?php echo base_url(); ?>dashboard/featuredlist_events">Featured Events</a></li>
            <li><a href="<?php echo base_url(); ?>dashboard/featuredlist_ads">Featured Ads</a></li>
<!--            <li><a href="--><?php //echo base_url(); ?><!--dashboard/featuredlist_prof">Featured List Profile</a></li>-->
        </ul>

        <br>
        <div class="info-user">
            <span aria-hidden="true" class="li_user fs1"></span>
            <span aria-hidden="true" class="li_settings fs1"></span>
            <span aria-hidden="true" class="li_mail fs1"></span>
            <span aria-hidden="true" class="li_key fs1"></span>
        </div>
    </div>
</div>

<div class="col-sm-3 col-lg-3">
    <div class="dash-unit">
        <div><dtitle>Preloaded data settings</dtitle></div>
        <hr>

        <ul>

            <li><a href="<?php echo base_url(); ?>dashboard/usertype">User Type/Category</a></li>
            <li><a href="<?php echo base_url(); ?>dashboard/stringinstrument"><i class="menu-icon icon-inbox"></i>String Instrument</a></li>
            <li><a href="<?php echo base_url(); ?>dashboard/reedinstrument"><i class="menu-icon icon-inbox"></i>Reed Instrument</a></li>
            <li><a href="<?php echo base_url(); ?>dashboard/windinstrument"><i class="menu-icon icon-inbox"></i>Wind Instrument</a></li>
            <li><a href="<?php echo base_url(); ?>dashboard/genre"><i class="menu-icon icon-inbox"></i>Genre</a></li>
            <li><a href="<?php echo base_url(); ?>dashboard/vocal"><i class="menu-icon icon-inbox"></i>Vocal</a></li>
            <li><a href="<?php echo base_url(); ?>dashboard/locations/add/0"><i class="menu-icon icon-inbox"></i>Location</a></li>

        </ul>


        <br>
        <div class="info-user">
            <span aria-hidden="true" class="li_user fs1"></span>
            <span aria-hidden="true" class="li_settings fs1"></span>
            <span aria-hidden="true" class="li_mail fs1"></span>
            <span aria-hidden="true" class="li_key fs1"></span>
        </div>
    </div>
</div>

<div class="col-sm-3 col-lg-3">
    <div class="dash-unit">
        <div><dtitle>Content Controls</dtitle></div>
        <hr>
        <ul>
            <li><a href="<?php echo base_url(); ?>dashboard/evlist?q=2&next=1&clicks=0&page_number=0">News/Events</a></li>
            <li><a href="<?php echo base_url(); ?>admin/profileList?q=1&next=1&clicks=0&page_number=0">Profiles</a></li>
            <li><a href="<?php echo base_url(); ?>dashboard/adlist?q=2&next=1&clicks=0&page_number=0">Ads</a></li>
        </ul>

        <br>
        <div class="info-user">
            <span aria-hidden="true" class="li_user fs1"></span>
            <span aria-hidden="true" class="li_settings fs1"></span>
            <span aria-hidden="true" class="li_mail fs1"></span>
            <span aria-hidden="true" class="li_key fs1"></span>
        </div>
    </div>
</div>

<div class="col-sm-3 col-lg-3">
    <div class="dash-unit">
        <div><dtitle>Search Settings</dtitle></div>
        <hr>
        <ul>
            <li><a>List Editing</a></li>
            <li><a>Keyword Tagging</a></li>
            <li><a>Keyword Indexing</a></li>
            <li><a>Search Pattern Settings</a></li>
        </ul>

        <br>
        <div class="info-user">
            <span aria-hidden="true" class="li_user fs1"></span>
            <span aria-hidden="true" class="li_settings fs1"></span>
            <span aria-hidden="true" class="li_mail fs1"></span>
            <span aria-hidden="true" class="li_key fs1"></span>
        </div>
    </div>
</div>

<div class="col-sm-3 col-lg-3">
    <div class="dash-unit">
        <div><dtitle>Message Controls</dtitle></div>
        <hr>
        <ul>
            <li><a>Mailing list extraction/generation</a></li>
            <li><a>Batch mail from template</a></li>
            <li><a>Batch sms</a></li>
        </ul>

        <br>
        <div class="info-user">
            <span aria-hidden="true" class="li_user fs1"></span>
            <span aria-hidden="true" class="li_settings fs1"></span>
            <span aria-hidden="true" class="li_mail fs1"></span>
            <span aria-hidden="true" class="li_key fs1"></span>
        </div>
    </div>
</div>

<div class="col-sm-3 col-lg-3">
    <div class="dash-unit">
        <div><dtitle>Subscription Manager</dtitle></div>
        <hr>

        <ul>
            <li><a>Rates and schemes</a></li>
            <li><a>Audit reports</a></li>

        </ul>

        <br>
        <div class="info-user">
            <span aria-hidden="true" class="li_user fs1"></span>
            <span aria-hidden="true" class="li_settings fs1"></span>
            <span aria-hidden="true" class="li_mail fs1"></span>
            <span aria-hidden="true" class="li_key fs1"></span>
        </div>
    </div>
</div>

<div class="col-sm-3 col-lg-3">
    <div class="dash-unit">
        <div><dtitle>Reports & Queries</dtitle></div>
        <hr>

        <ul>
            <li><a>Profile matching-query based on seeking</a></li>
            <li><a>Mailing list extraction/generation</a></li>
            <li><a>Transactional reports</a></li>
        </ul>

        <br>
        <div class="info-user">
            <span aria-hidden="true" class="li_user fs1"></span>
            <span aria-hidden="true" class="li_settings fs1"></span>
            <span aria-hidden="true" class="li_mail fs1"></span>
            <span aria-hidden="true" class="li_key fs1"></span>
        </div>
    </div>
</div>

<div class="col-sm-3 col-lg-3">
    <div class="dash-unit">
        <div><dtitle>Support Chat</dtitle></div>
        <hr>
        <!--        <div class="thumbnail">-->
        <!--            <img src="images/face80x80.jpg" alt="Marcel Newman" class="img-circle">-->
        <!--        </div><!-- /thumbnail -->
        <!--        <h1>Marcel Newman</h1>-->
        <!--        <h3>Madrid, Spain</h3>-->



        <br>
        <div class="info-user">
            <span aria-hidden="true" class="li_user fs1"></span>
            <span aria-hidden="true" class="li_settings fs1"></span>
            <span aria-hidden="true" class="li_mail fs1"></span>
            <span aria-hidden="true" class="li_key fs1"></span>
        </div>
    </div>
</div>

<div class="col-sm-3 col-lg-3">
    <div class="dash-unit">
        <div><dtitle>Batch Upload</dtitle></div>
        <hr>
        <ul>
            <li><?php echo anchor('batch/updatefb','Update Facebook Count'); ?></li>
            <li><?php echo anchor('batch/updateyoutube','Update Youtube Count'); ?></li>
        </ul>

        <br>
        <div class="info-user">
            <span aria-hidden="true" class="li_user fs1"></span>
            <span aria-hidden="true" class="li_settings fs1"></span>
            <span aria-hidden="true" class="li_mail fs1"></span>
            <span aria-hidden="true" class="li_key fs1"></span>
        </div>
    </div>
</div>
<!--<div class="col-sm-3 col-lg-3">-->
<!--    <div class="dash-unit">-->
<!--        <div><dtitle>Featured Category</dtitle></div>-->
<!--        <hr>-->
<!---->
<!--        <ul>-->
<!--            <li><a href="--><?php //echo base_url(); ?><!--batch/featuredProfiles">Featured Profiles</a></li>-->
<!--            <!--<li><a href="">Subscription Status</a></li>-->
<!--        </ul>-->
<!---->
<!--        <br>-->
<!--        <div class="info-user">-->
<!--            <span aria-hidden="true" class="li_user fs1"></span>-->
<!--            <span aria-hidden="true" class="li_settings fs1"></span>-->
<!--            <span aria-hidden="true" class="li_mail fs1"></span>-->
<!--            <span aria-hidden="true" class="li_key fs1"></span>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

