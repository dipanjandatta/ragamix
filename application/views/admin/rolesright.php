
<?php $this->load->view('layouts/admin/main'); ?>


<div class="row ng-scope">

    <?php
    echo form_open('dashboard/rolesright');?>

    <div class="col-lg-6">
        <srd-widget>
            <div class="widget" >
                <srd-widget-header icon="fa-tasks" title="Servers" class="ng-scope ng-isolate-scope">
                    <div class="widget-header ng-binding" style="font-size: 24px; font-weight: bold;text-align: center">
                        User Group

                    </div>
                </srd-widget-header>
                <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope" >

                    <div style="border: 1px solid #ffffff; padding: 6%;    margin-left: 40px;">

                        <div ng-hide="loading" class="widget-content" >
                            <div class="form-horizontal divpadding" >

                                <div class="form-group form-group-lg othpadding">
                                    <label class="col-sm-4 control-label labelcolor" for="lg">Group Name</label>
                                    <div class="col-sm-8">

                                        <select class="form-control"  name="selval">
                                            <option value="">Select Group</option>
                                            <?php foreach($rolesrightdata as $val):?>
                                            <option  value="<?php echo $val->group_id?>" selected="selected"><?php echo $val->group_name?></option>
                                            <?php endforeach;?>
                                        </select>

                                    </div>
                                </div>

                            </div>

                        </div>
<!--                        <button style="float:right" type="submit" value="result" name="result"/> result-->
                        <div style="float: right">
<!--                        --><?php //echo anchor('dashboard/rolesright/edit','Result', array('class'=>'btn btn-primary')); ?>
                                                    <button style="float:right" type="submit" value="result" name="result"/> result

                        </div>

                    </div>


                    <div style="border: 1px solid #ffffff; padding: 6%;    margin-left: 40px;">

                        <div ng-hide="loading" class="widget-content" >
                            <div class="table-responsive1 ng-scope form-horizontal">
                                <div class="form-group form-group-lg toppadding">
                                    <label class="col-sm-4 control-label" for="lg">Mode</label>
                                    <div class="col-sm-8">
                                        <select name="insertassign" class="form-control" id="mySelect" onchange="myFunction()">
                                            <option value="">Select Mode</option>
                                            <option value="insert">Insert Group</option>
<!--                                            <option value="2">Update Group</option>-->
                                            <option value="assign">Assign Group</option>
                                        </select>
                                    </div>
<!--                                    <button type="submit" style="float: right" name="change" value="change"/>change-->
                                </div>
                                <div id="demo" class="form-group form-group-lg othpadding" >
                                    <label class="col-sm-4 control-label" for="lg">Group Name</label>
                                    <div class="col-sm-8">

                                        <!--<input class="form-control" type="text"   ng-model="usergroup_data.group_id" >-->
                                        <select class="form-control" name="selgroup">
                                            <option value="<?php echo $val->group_name?>">Select Group</option>
                                            <?php foreach($rolesrightdata as $val):?>
                                            <option value="<?php echo $val->group_id?>"><?php echo $val->group_name?></option>
                                            <?php endforeach;?>
                                        </select>

                                    </div>
                                </div>

                                <div id="demo1" class="form-group form-group-lg othpadding">
                                    <label class="col-sm-4 control-label" for="lg">Group Name</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="textsel" type="text">
                                    </div>
                                </div>


                                <div class="form-group form-group-lg othpadding">
                                    <label class="col-sm-4 control-label" for="lg">User Type</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" type="text" name="usertypeo">
                                    </div>
                                </div>
<!--                                <div class="form-group form-group-lg othpadding">-->
<!--                                    <label class="col-sm-4 control-label" for="lg">Group Description</label>-->
<!--                                    <div class="col-sm-8">-->
<!--                                        <input class="form-control" type="text" id="lg" ng-model="group.group_desc">-->
<!---->
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="form-group form-group-lg othpadding">
                                    <label class="col-sm-4 control-label" for="lg">Users</label>
                                    <div class="col-sm-8">
                                        <!--<input class="form-control" type="text"   ng-model="usergroup_data.group_id" >-->
                                        <select id="demo2" name="heavy[]" class="form-control" multiple  disabled>
                                            <?php foreach($rolesuserdata as $val):?>
                                                <option value="<?php echo $val->rm_id?>"><?php echo $val->user_name?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>


                                <div class="pull-right othpadding" >
                                    <button type="submit" name="save" value="save" class="btn btn-sm btn-info margin7 ng-scope">Save</button>
                                    <!--<button class="btn btn-sm btn-info margin7 ng-scope" ng-click="addnew_distributor_data();">Add New</button>-->
                                    <button class="btn btn-sm btn-info margin7 ng-scope">Cancel</button>

                                </div>


                            </div>

                        </div>

                    </div>

                </srd-widget-body>

            </div>
        </srd-widget>
    </div>
    <div class="col-lg-6">
        <srd-widget>
            <div class="widget" style="border: 1px solid #ffffff;margin-right: 5%; margin-top: 5%;">
                <srd-widget-body classes="medium no-padding" class="ng-scope ng-isolate-scope">
                    <div class="widget-body medium no-padding" style="height: 500px; overflow: scroll" ng-class="classes">

                        <div ng-hide="loading" class="widget-content" >
                            <div class="table-responsive1 ng-scope">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th >Users</th>
<!--                                        <th>Status</th>-->

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($roles as $val):?>
                                    <tr>

                                        <td height="5px"><?php echo $val->user_name?></td>

                                    </tr>
                                    <?php endforeach;?>
<!--                                        <td class="text-left" height="5px">-->
<!--                                            <input type="checkbox" ng-model="rgts.rights_id" ng-true-value=true ng-false-value=false ng-click="right_check(rgts,rgts.rights_id)">-->
<!--                                            </td>-->
                                            <!--<input type="checkbox" name="id" ng-model="rgts.right"  ng-click="right_check(rgts.right,rgts)">
                                        </td>

<!--                                    </tr>-->

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </srd-widget-body>

            </div>
        </srd-widget>
    </div>

    <?php form_close()?>
</div>

<script>
    function myFunction() {
        var x = document.getElementById("mySelect").value;
    if(x=="insert"){
        document.getElementById('demo').style.visibility = 'hidden';
        document.getElementById('demo1').style.visibility = 'visible';
        document.getElementById('demo2').disabled = true;

    }
        if(x=="assign"){
            document.getElementById('demo').style.visibility = 'visible';
            document.getElementById('demo1').style.visibility = 'hidden';
            document.getElementById('demo2').disabled = false;
        }

    }
</script>