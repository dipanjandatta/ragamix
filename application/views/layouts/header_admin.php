<?php // $this->output->cache(10); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <title>Welcome To Ragamix</title>
    <?php $this->load->view('assets/css'); ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/modal.css" type="text/css" media="screen">
    <!--    --><?php //$this->load->view('assets/js'); ?>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.src.js"></script>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <![endif]-->

</head>
<header id="navigation">

    <div class="navbar" role="banner">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" >
                            <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
                            <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                        <a class="navbar-brand" href=""><img src="../images/admin/logo.png" height="70" alt="logo"></a>
                    </div>
                </div>
                <div class="col-sm-9"> <nav class="navbar-right collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                            <li><a href="">LOGOUT</a></li>

                            <!--                            <li><a href="#/main/batchprocess">Batch Process</a></li>-->
                            <!--                            <li><a href="--><?php //echo base_url(); ?><!--admin/batchprocess"><i class="menu-icon icon-inbox"></i>Batch ProceSS</a></li>-->


                            <!--                            <li><a href="--><?php //echo base_url(); ?><!--admin/activity_console">Activity Log</a></li>-->
                            <!--                            <li><a href="--><?php //echo base_url(); ?><!--admin/rolesright">USER</a></li>-->
                            <!--                            <li><a href="--><?php //echo base_url(); ?><!--admin/group_vs_user" >GROUP VS USER</a></li>-->
                            <!--                            <li><a class='gsubmenu' href=" ">BROWSE <i style="margin-left:5px" class="fa fa-chevron-down"></i></a>-->
                            <!--                                <ul class="sub-menu" role="menu" >-->
                            <!--                                    <li><a href="">Profile</a></li>-->
                            <!--                                    <li><a href="">Classified Ads</a></li>-->
                            <!--                                    <li><a href="">Events</a></li>-->
                            <!--                                </ul>-->
                            <!--                            </li>-->

                            <!---->
                            <!--                            <li><a class='gsubmenu' href="">ADMINISTRATION <i style="margin-left:5px" class="fa fa-chevron-down"></i></a>-->
                            <!--                                <ul class="sub-menu" role="menu" >-->
                            <!---->
                            <!--                                    <li><a href="--><?php //echo base_url(); ?><!--admin/usertype"><i class="menu-icon icon-inbox"></i>User Type</a></li>-->
                            <!--                                    <li><a href=""><i class="menu-icon icon-inbox"></i>Percussions</a></li>-->
                            <!--                                    <li><a href="--><?php //echo base_url(); ?><!--admin/stringinstrument"><i class="menu-icon icon-inbox"></i>String Instrument</a></li>-->
                            <!--                                    <li><a href="--><?php //echo base_url(); ?><!--admin/reedinstrument"><i class="menu-icon icon-inbox"></i>Reed Instrument</a></li>-->
                            <!--                                    <li><a href="--><?php //echo base_url(); ?><!--admin/windinstrument"><i class="menu-icon icon-inbox"></i>Wind Instrument</a></li>-->
                            <!--                                    <li><a href="--><?php //echo base_url(); ?><!--admin/genre"><i class="menu-icon icon-inbox"></i>Genre</a></li>-->
                            <!--                                    <li><a href="--><?php //echo base_url(); ?><!--admin/vocal"><i class="menu-icon icon-inbox"></i>Vocal</a></li>-->
                            <!--                                    <li><a href="--><?php //echo base_url(); ?><!--admin/city"><i class="menu-icon icon-inbox"></i>City</a></li>-->
                            <!--                                    <li><a href="<?php //echo base_url(); ?><!--admin/music"><i class="menu-icon icon-inbox"></i>Music</a></li>
<!---->
                            <!--                                </ul>-->
                            <!--                            </li>-->
                        </ul>
                        <!--<div class="placedate"><span id="place">Kolkata, </span><span id="time"></span></div>-->
                    </nav> </div>
            </div>
        </div>
    </div>
</header>
</html>
<!-- MENU SECTION END-->
