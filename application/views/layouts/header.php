<?php 
$CI =& get_instance();
$CI->load->library('user_agent');
$CI->load->helper('cookie');
//print_r($CI->agent->is_mobile());exit();
if($CI->agent->is_mobile()){ //Change it to true when deploying ?>
<div class="col-md-12 topHeader">
    <div class="container">
        <div class="mobileLeft">
            <a href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url(); ?>images/ragamixlatest.png" class="pogomobile" />
            </a>
        </div>
        <div class="mobileRight">
            <?php if(!$this->session->userdata('user_id')) { ?>
            <button type="button" class="btn mobbtnin su" id="su" style="background: #232f3e;border-right: 1px solid;padding-right: 7px;">Sign In</button>
            <button type="button" class="btn mobbtnin si" id="si" style="background: #232f3e;">Sign Up</button>
            <?php } else { $returnValue = htmlspecialchars($this->session->userdata('profile_pic')); ?>
            <div class="headerProfileHolder mobileHeaderSmallMargRight">
                <div class="dropdown">
                    <a class="account">
                        <div style='height: 40px; width: 50px; background-color: #e9ebee; background-position: center 40%; background-size: cover; background-repeat: no-repeat;display: block; background-image: url(<?php echo $returnValue; ?>);'></div>
                    </a>
                    <div class="submenu submenuMobile" style="display: none; ">
<!--                        <div class="headerName">
                            MESSAGES
                        </div>
                        <ul class="root">
                            <li>
                                <a href="#">
                                    You have 0 messages
                                </a>
                            </li>
                        </ul>-->
                        <div class="headerName">
                            SETTINGS
                        </div>
                      <ul class="root">
                          <li>
                              <?php echo anchor('profile/profileEdit/basic', 'Basic Profile'); ?>
                          </li>
                          <li>
                              <?php echo anchor('profile/profileEdit/music', 'Music Profile'); ?>
                          </li>
                          <li class="bots"></li>
                          <li >
                            <?php echo anchor('profile/profileEdit/pg', 'Manage Photo Gallery'); ?>
                          </li>


                        <li>
                          <?php echo anchor('profile/profileEdit/audiovideo', 'Manage Audio / Video'); ?>
                        </li>
<!--                        <li>

                          <?php echo anchor('#', 'Manage Subscriptions'); ?>
                        </li>
                        <li class="bots"></li>

                        <li>
                          <?php echo anchor('#', 'Post Announcements'); ?>
                        </li>-->

                        <li>
                          <?php echo anchor('profile/profileEdit/showevents', 'Post Shows / Events'); ?>
                        </li>
                        <li>
                          <?php echo anchor('profile/profileEdit/postads', 'Post Ads'); ?>
                        </li>
                        <li>
                          <?php echo anchor('profile/profileEdit/musicresource', 'Post Seek Musicians'); ?>
                        </li>
                        <li class="bots"></li>
<!--                        <li>
                          <?php echo anchor('#', 'Mail / SMS Settings'); ?>
                        </li>
                        <li>
                          <?php echo anchor('#', 'Password Settings'); ?>
                        </li> -->
                        <li class="bots"></li>
                        <li>
                          <?php echo anchor('welcome/logout', 'Sign Out'); ?>
                        </li>
                      </ul>
                    </div>
                </div>
            </div>
            <div class="headerProfileHolder">
                <i class="fa fa-th iconHead" id="notser" style="cursor: pointer;position: absolute;left: 167px;top: 2px;"></i>
                <div id="notifications">
                    <div class="col-md-12 noPad">
                        <?php echo anchor('events/view', '<div class="col-md-3 blocks colgreen"><i class="fa fa-table iconMenu"></i><span class="ev">Events</span></div>'); ?>
                        <?php echo anchor('musicians/singers', '<div class="col-md-3 blocks colorange"><i class="fa fa-microphone iconMenu"></i><span class="ev">Singers</span></div>'); ?>
                        <div class="col-md-3 blocks colblue">
                            <i class="fa fa-graduation-cap iconMenu"></i>
                            <span class="ev">
                                Educators
                            </span>
                        </div>
                        <div class="col-md-3 blocks colpurple">
                            <i class="fa fa-television iconMenu"></i>
                            <span class="ev">
                                News
                            </span>
                        </div>
                        <?php echo anchor('musicians/view', '<div class="col-md-3 blocks colblue"><i class="fa fa-pied-piper-alt iconMenu"></i><span class="ev">Musicians</span></div>'); ?>
                        <?php echo anchor('ads/view', '<div class="col-md-3 blocks colred"><i class="fa fa-opencart iconMenu"></i><span class="ev">MarketPlace</span></div>'); ?>
                        <div class="col-md-3 blocks colbrown">
                            <i class="fa fa-film iconMenu"></i>
                            <span class="ev">
                                Albums
                            </span>
                        </div>
                        <div class="col-md-3 blocks colgreen">
                            <i class="fa fa-line-chart iconMenu"></i>
                            <span class="ev">
                                Trending
                            </span>
                        </div>
                        <div class="col-md-3 blocks colgarnish">
                            <i class="fa fa-newspaper-o iconMenu"></i>
                            <span class="ev">
                                Reviews
                            </span>
                        </div>
                        <?php echo anchor('musicians/band', '<div class="col-md-3 blocks colgarnishorange"><i class="fa fa-magic iconMenu"></i><span class="ev">Bands</span></div>'); ?>
                        <div class="col-md-3 blocks colmagenta">
                            <i class="fa fa-paper-plane-o iconMenu"></i>
                            <span class="ev">
                                Promoters
                            </span>
                        </div>
                        <div class="col-md-3 blocks colbresh">
                            <i class="fa fa-database iconMenu"></i>
                            <span class="ev">
                                Archives
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php } else { ?>
<div class="col-md-12 topHeader">
    <div class="container">
        <div class="col-md-2 noPad">
            <i class="fa fa-th iconHead" id="notser" style="cursor: pointer;position: absolute;left: 167px;top: 2px;"></i>
                <!--THE NOTIFICAIONS DROPDOWN BOX.-->
                <div id="notifications" style="left: 158px !important;">
                    <div class="col-md-12 noPad">
                        <?php echo anchor('events/view', '<div class="col-md-3 blocks colgreen"><i class="fa fa-table iconMenu"></i><span class="ev">Events</span></div>'); ?>
                        <?php echo anchor('musicians/singers', '<div class="col-md-3 blocks colorange"><i class="fa fa-microphone iconMenu"></i><span class="ev">Singers</span></div>'); ?>
                        <div class="col-md-3 blocks colblue">
                            <i class="fa fa-graduation-cap iconMenu"></i>
                            <span class="ev">
                                Educators
                            </span>
                        </div>
                        <?php echo anchor('events/view', '<div class="col-md-3 blocks colpurple"><i class="fa fa-television iconMenu"></i><span class="ev">News</span></div>'); ?>
                            <?php echo anchor('musicians/view', '<div class="col-md-3 blocks colblue"><i class="fa fa-pied-piper-alt iconMenu"></i><span class="ev">Musicians</span></div>'); ?>
                            <?php echo anchor('ads/view', '<div class="col-md-3 blocks colred"><i class="fa fa-opencart iconMenu"></i><span class="ev">MarketPlace</span></div>'); ?>
<!--                        <div class="col-md-3 blocks colbrown">
                            <i class="fa fa-film iconMenu"></i>
                            <span class="ev">
                                Albums
                            </span>
                        </div>-->
<!--                        <div class="col-md-3 blocks colgreen">
                            <i class="fa fa-line-chart iconMenu"></i>
                            <span class="ev">
                                Trending
                            </span>
                        </div>-->
<!--                        --><?php //echo anchor('events/view', '<div class="col-md-3 blocks colgarnish"><i class="fa fa-film iconMenu"></i><span class="ev">Reviews</span></div>'); ?>
                        <?php echo anchor('seek/view?next=1&clicks=0&page_number=0', '<div class="col-md-3 blocks colgarnish"><i class="fa fa-film iconMenu"></i><span class="ev">Jobs</span></div>'); ?>
                        <?php echo anchor('musicians/band', '<div class="col-md-3 blocks colgarnishorange"><i class="fa fa-magic iconMenu"></i><span class="ev">Bands</span></div>'); ?>
<!--                        <div class="col-md-3 blocks colmagenta">
                            <i class="fa fa-paper-plane-o iconMenu"></i>
                            <span class="ev">
                                Promoters
                            </span>
                        </div>-->
<!--                        <div class="col-md-3 blocks colbresh">
                            <i class="fa fa-database iconMenu"></i>
                            <span class="ev">
                                Archives
                            </span>
                        </div>-->
                    </div>
                </div>
            <a href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url(); ?>images/web_logo4.png" class="pogo" style="max-width: 70%;margin-top: 3px;"/>
<!--                <span class="logse">-->
<!--                    Ragamix <i class="fa fa-music"></i>-->
<!--                </span>-->
            </a>
        </div>
        <div class="col-md-7 noPad">
            <div id="searchContainer"></div>
        </div>
        <div class="col-md-3 noPad" style="text-align: right;">
        <?php if(!$this->session->userdata('user_id')) { ?>
            <a class="btn btnssin su" style="border-right: 1px solid;">Sign In</a>
            <a class="btn btnssin si" id="si">Sign Up</a>
        <?php } else {  ?>
            <div id="msgcnt"></div>
            <a class="account userBlock">
                <div class="userf">
                    <span class="caret"></span>&nbsp;&nbsp;
                        <?php
                            if(strlen($this->session->userdata('user_name')) > 15){
                                echo substr($this->session->userdata('user_name'),0,15).'..';
                            }
                            else {
                                echo $this->session->userdata('user_name');
                            }
                        ?>
                </div>
                <?php
                    $returnValue = htmlspecialchars($this->session->userdata('profile_pic'));
                ?>
                <div class="headerProfileHolder">
                    <div style='height: 35px; width: 35px; border-radius: 50px;background-color: #e9ebee; background-position: center 40%; background-size: cover; background-repeat: no-repeat;display: block; background-image: url(<?php echo $returnValue; ?>);'></div>
                </div>
            </a>
        <?php } ?>    
            <div class="dropdown">
                <?php if(!$this->session->userdata('user_id')) { ?>

                <a class="account">
                    <i class="fa fa-cog"></i>
                </a>
                <div class="submenu" style="display: none; ">
                    <div class="headerName">
                        SETTINGS
                    </div>
                  <ul class="root">

                      <li>
                          <?php echo anchor('profile/profileEdit/basic', 'Basic Profile'); ?>
                      </li>
                      <li>
                          <?php echo anchor('profile/profileEdit/music', 'Music Profile'); ?>
                      </li>
                      <li class="bots"></li>
                      <li >
                        <?php echo anchor('profile/profileEdit/photo', 'Manage Photo Gallery'); ?>
                      </li>


                    <li>
                      <?php echo anchor('profile/profileEdit/audiovideo', 'Manage Audio / Video'); ?>
                    </li>

                    <li>
                      <?php echo anchor('profile/profileEdit/showevents', 'Post Shows / Events'); ?>
                    </li>
                    <li>
                      <?php echo anchor('profile/profileEdit/postads', 'Post Ads'); ?>
                    </li>
                    <li>
                      <?php echo anchor('profile/profileEdit/musicresource', 'Post Seek Musicians'); ?>
                    </li>

                    <li class="bots"></li>
                    <li>
                      <?php echo anchor('welcome/logout', 'Sign Out'); ?>
                    </li>
                  </ul>
                </div>
                <?php } else { ?>
                <div class="submenu" style="display: none; left: 0 !important;">
                    <div class="headerName">
                        SETTINGS
                    </div>
                  <ul class="root">
                <?php if($this->session->userdata('user_type_id')==3){?>
                      <li>
                          <?php echo anchor('dashboard/view', 'Dashboard'); ?>
                      </li>
                      <?php }else {?>
                      <?php }?>

                      <li>
                          <?php echo anchor('profile/profileEdit/basic', 'Basic Profile'); ?>
                      </li>
                      <li>
                          <?php echo anchor('profile/profileEdit/music', 'Music Profile'); ?>
                      </li>
                      <li class="bots"></li>
                      <li >
                        <?php echo anchor('profile/profileEdit/photo', 'Manage Photo Gallery'); ?>
                      </li>


                    <li>
                      <?php echo anchor('profile/profileEdit/audiovideo', 'Manage Audio / Video'); ?>
                    </li>

                    <li>
                      <?php echo anchor('profile/profileEdit/showevents', 'Post Shows / Events'); ?>
                    </li>
                    <li>
                      <?php echo anchor('profile/profileEdit/postads', 'Post Ads'); ?>
                    </li>
                    <li>
                      <?php echo anchor('profile/profileEdit/musicresource', 'Post Seek Musicians'); ?>
                    </li>
                      <li class="bots"></li>
                      <li>
                          <?php echo anchor('welcome/settings', 'Change Password'); ?>
                      </li>
                    <li class="bots"></li>
                    <li>
                      <?php echo anchor('welcome/logout', 'Sign Out'); ?>
                    </li>
      			<li>
                          <?php echo anchor($CI->config->item('chat_url').'public/user.php?source=nuser&un='.$this->session->userdata('user_name'), 				'Support Chat', array('target'=>'_blank')); ?>
                      </li>
                  </ul>
                </div>
                <?php } ?>
                </div>
        </div>
    </div>
</div>
<?php } ?>

<div class="splashers" style="display: none;">
  <div class="splash-inner">
    <div class="col-md-12" style="text-align: right;">
      <a class="si"><i class="fa fa-times-circle-o bigFontIcons"></i> </a>
    </div>
    <?php 
          $foo = $CI->input->cookie('user',true);
          $bar = $CI->input->cookie('user_img',true);
		  $returnValueOne = htmlspecialchars($bar);
    		if(empty($foo)){
	?>
      <div class="col-md-4-5 col-md-offset-4" style="padding-top: 25px;">
        <div class="panel panel-default noBord">
          <div class="panel-heading panelForm">
            Login
          </div>
            <?php echo form_open('welcome/login'); ?>
          <div class="panel-body panForm nanan">
            <div class="signform" id="business">
              <div class="control-group form-group ragaformarFix">
                <div class="controls">
                  <input type="text" name="loginId" class="form-control ragacontrols" placeholder="Email / Username">
                </div>
              </div>
              <div class="control-group form-group ragaformarFix">
                    <div class="controls">
                        <input type="password" name="user_password" class="form-control ragacontrols" placeholder="Password">
                    </div>
                </div>
                <div class="control-group form-group ragaformarFix">
                    <a data-toggle="modal" data-target="#myModal" style="cursor: pointer;">Forgot Password</a>
<!--                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>-->

                    <div class="controls col-sm-6 noPad" style="float: right; text-align: right;     margin: 5px 0px 30px 0px;">
                      <button type="submit" class="btn btnfix greenbtn" style="padding: 7px 50px !important;">LOGIN</button>
                    </div>
                </div>
            </div>
          </div>
      </div>
      </div>
      <?php 
		}
		else {
	  ?>
	  <div class="container">
      <div class="col-md-5" style="padding-top: 25px;">
        <div class="panel panel-default noBord">
          <div class="panel-heading panelForm">
            Login
          </div>
            <?php echo form_open('welcome/login'); ?>
          <div class="panel-body panForm nanan">
            <div class="signform" id="business">
              <div class="control-group form-group ragaformarFix">
                <div class="controls">
                  <input type="text" name="loginId" class="form-control ragacontrols" placeholder="Email / Username">
                </div>
              </div>
              <div class="control-group form-group ragaformarFix">
                    <div class="controls">
                        <input type="password" name="user_password" class="form-control ragacontrols" placeholder="Password">
                    </div>
                </div>
                <div class="control-group form-group ragaformarFix">
                    <div class="controls col-sm-6 noPad" style="float: right; text-align: right;     margin: 5px 0px 30px 0px;">
                      <button type="submit" class="btn btnfix greenbtn" style="padding: 7px 50px !important;">LOGIN</button>
                    </div>
                </div>
            </div>
          </div>

            <div id = "myDiv"  class="loader" style="display:none;opacity: 0.5;"><img id = "myImage" ></div>

            <?php echo form_close(); ?>
      </div>
      </div>
      <div class="col-md-2 col-md-offset-1">
      	<div class="verticleImage"></div>
      </div>
      <?php echo form_open('welcome/login'); ?>
      <div class="col-md-4" style="padding-top: 25px;">
      	<a class="showAni">
      		<div class="oaa">
	      	<div class="oa" style='height: 200px; width: 200px; background-color: #e9ebee; background-position: center 40%; background-size: cover; background-repeat: no-repeat;display: block; background-image: url(<?php echo $returnValueOne; ?>);'></div>
	      	<span class="cookName oa">
	      		<?php echo $foo; ?>
	      	</span>
	      	</div>
      	</a>
      	<div class="cookRem">
      		<?php
      			// $arr = array(
      							// 'cn' => $foo,
      							// 'cim' => $returnValueOne
      			// ); 
				// $query = http_build_query($arr);
      			echo anchor('welcome/remtemp', '<i class="fa fa-times-circle-o"></i>', array('class'=>'recon')); 
      		?>
      	</div>
      	<div class="op">
      		
              <div class="control-group form-group ragaformarFix">
                    <div class="controls">
                    	<input type="hidden" name="loginId" value="<?php echo $foo; ?>">
                        <input type="password" name="user_password" class="form-control ragacontrols" placeholder="Enter Password">
                    </div>
                </div>
                <div class="control-group form-group ragaformarFix">
                    <div class="controls col-sm-6 noPad" style="    width: 100% !important;">
                      <button type="submit" class="btn btnfix greenbtn" style="padding: 7px 50px !important;     width: 100%;">LOGIN</button>
                    </div>
                </div>
           
		</div>
		
      </div>
      <?php echo form_close(); ?>
	</div>
      <?php
      	}
	  ?>
      </div>
</div>

<div class="modal fade" id="myModal" role="dialog" style=" z-index: 9999999;">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Please Provide email id</h4>
            </div>
            <div class="modal-body">
                <input type="text" id="mailid" placeholder="provide email id" > &nbsp;<input type="button" value="send" onclick="sendmail()" >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


