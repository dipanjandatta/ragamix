                <div id="nav-01" class="crsl-nav">
                    <a href="#" class="previous"><i class="fa fa-chevron-left iconArrow"></i></a>
                    <a href="#" class="next"><i class="fa fa-chevron-right iconArrow"></i></a>
		</div>
                    <div class="crsl-items" data-navigation="nav-01">
                        <div class="crsl-wrap">
                    <?php foreach($reviews as $val): ?>
                            <figure class="crsl-item">
<a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$val->category); ?>/<?php echo $val->event_ad_id; ?>">
                  <div class="col-md-2-3 uniPad mustWidth">
                    <div class="demo-section k-content">
                        <div class="coverClass blurim">
                            <img src="<?php echo $val->pic_url;?>" class="newImg" />
                        </div>
                      <div class="title grey abpos">
                        <?php echo $val->event_title; ?>
                      </div>
                    </div>
                  </div>
</a>
                            </figure>
                  <?php endforeach; ?>
                        </div>
                    </div>
<script type="text/javascript">
    $(document).ready(function(){
	$('.crsl-items').carousel({ visible: 4, itemMinWidth: 180, itemMargin: 1 }); 
    });
</script>