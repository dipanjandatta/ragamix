<div class="container stanPad">
    <div class="row blog-row bg-color-white" style="padding: 10px;">
        <div class="col-xs-8 col-sm-8 col-md-8" >

            <div class="col-md-12" style="background: white;padding: 18px 25px 0px 40px;">

                <div class="row">
                    <div class=" blog-content" style="padding: 1px 0px 2px 8px !important;font-size: 18px;">
                        <?php echo $keyword; ?>
                    </div>
                    <div class=" blog-content" style="padding: 1px 0px 2px 8px !important;">
                        <!--                <div style="width:10px;height: 10px">-->
                        <!--                <img style="height: 10px;width:10px" src="--><?php //echo $profile_pic_url; ?><!--">-->
                        <!--                </div>-->
                        <?php echo $description; ?>
                    </div>
                    <div class=" blog-title" style="padding: 11px 0px 13px 8px !important;">
                        <div style="font-size: 12px;position: absolute;left: 36px;width: 180px;">
                    <span class="offw" style="padding: 0px !important;">
                          <div class="headerProfileHolder">
                              <div style="height: 35px; width: 35px; border-radius: 50px;background-color: #e9ebee; background-position: center 40%; background-size: cover; background-repeat: no-repeat;display: block;margin-right: 10px;margin-top: 7px;; background-image: url(<?php echo $pic_url; ?>);"></div>
                          </div>
                        By </span><a style="font-size:15px;" href="<?php echo base_url(); ?>profile/details/musician/<?php echo $user_name; ?>"><?php echo $user_name; ?></a><br>
                            <span >Posted On&nbsp;:&nbsp;<?php echo $updated_on; ?></span>
                        </div>
                        <div class="fb-share-button" data-href="<?php echo current_url(); ?>" data-layout="button" data-mobile-iframe="true" style="float: left; left: 200px;top: 14px;"></div>

                    </div>

                </div>

                <div class="btn-group btn-group-justified m-b" style="margin-left: -20px;">
                    <div class="modal-body" style="width: 438px;margin-top: 26px;">
                        <?php
                        if($this->session->userdata('user_id')) {
                            $atr = array('style'=>'margin-top: 10px');
                            echo form_open('seek/sendmessagefromseek',$atr);
                            ?>
                            <textarea name="body_text" cols="5" rows="5" placeholder="Send a message to contact <?php echo $user_name; ?>" style="    background: rgba(211, 211, 211, 0.06); "></textarea>
                            <input type="hidden" name="toid" value="<?php echo $rmid; ?>" />
                            <input type="submit" name="submit" value="Post Message" class="uibutton large btn-success" style="width: 100%;background: green;color: white;" />
                            <?php
                            echo form_close();
                        }
                        else { ?>
                            <a data-toggle="modal" data-target="#sendmessage" class="uibutton large special"> Send a message</a>
                        <?php  }
                        ?>
                    </div>
                </div>

            </div>

        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 noPad" style="border: black 1px solid;">
            <div class="col-md-12 noPad">
                <div class="">
                    <div class="panel-heading panelSubMain" style="background: #fff5ce!important;color: #754646 !important;">
                        <a href="<?php echo base_url();?>seek/view?next=1&clicks=0&page_number=0">More Opportunities</a>
                    </div>
                    <div class="" style="height: 267px !important;">
                        <ul class="list-group" style="margin-bottom: 0px !important;">
                            <?php
                            foreach($seeklist as $val):

                               $returnValue = str_replace(' ', '%20', $val->profile_pic_url);

                                ?>


                                <li class="list-group-item adjustList bhover" style="margin-bottom: 12px !important;margin-left: 15px;margin-right: 15px;">
                                    <div >
                                        <a href="<?php echo base_url(); ?>seek/seekdetails/<?php echo $val->serial_no; ?>">
                                        <span class="coverClass searchProfImgHolderBig" style="height: 75px!important;">
                                            <div style="height:90px; max-width: 100%; background-size: cover; background-position: center 50%; background-color: #e9ebee; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
                                        </span>
<div>
                                        <span class="badge caser pacer" style="padding-left: 14px;">
                                            <?php echo $val->keyword; ?>
                                        </span>

                                        <span class="badge caser ashfont margBot" style="padding-left: 14px;width :75% !important">
                                            <?php echo substr($val->description, 0, 100)."..."; ?>
                                        </span>
                                        <span class="badge caser" style="padding-left: 14px;">

                                            By <?php echo $val->user_name; ?>

                                        </span>
</div>
                                        </a>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="sendmessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h1 class="modal-title">Send a Message</h1>
                </div>

                <div class="modal-body">
                    <?php
                    if($this->session->userdata('user_id')) {
                        echo form_open('profile/sendmessagefromprofile');
                        ?>
                        <textarea name="body_text" cols="38" rows="10" placeholder="Create a message"></textarea>
                        <input type="hidden" name="toid" value="<?php echo $rmid; ?>" />
                        <input type="submit" name="submit" value="Post Message" class="uibutton large" />
                        <?php
                        echo form_close();
                    }
                    else {
                        echo "Please Login to Send a message to ".$user_name;
                    }
                    ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if(@$err){ ?>
    <div class="error" style="position: absolute; background: red; width: 100%; text-align: center; padding: 20px; top: 100px;  z-index: 999999999 !important; color: white; font-size: 30px;">
        <?php echo @$err; ?>
    </div>
<?php } ?>	
<!--div class="blog-row-spacing"></div-->

<div class="modal fade" id="dologin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="padding: 0 !important;">
                <?php echo form_open('welcome/login'); ?>
                <div class="panel-body panForm nanan">
                    <div class="signform" id="business">
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls">
                                <input type="text" name="loginId" class="form-control ragacontrols" placeholder="Email / Username">
                            </div>
                        </div>
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls">
                                <input type="password" name="user_password" class="form-control ragacontrols" placeholder="Password">
                            </div>
                        </div>
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls col-sm-6 noPad" style="float: left; text-align: right;     margin: 5px 0px 3px 0px;">
                                <button type="submit" class="btn btnfix greenbtn" style="padding: 7px 45px !important;">LOGIN</button>
                            </div>
                            <div class="controls col-sm-6 noPad" style="float: right; text-align: right;     margin: 5px 0px 3px 0px;">
                                <button type="button" class="btn btnfix greenbtn" style="padding: 7px 40px !important;" data-dismiss="modal">CANCEL</button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                echo form_close();
                ?>
                <!--div class="socialLoginBarrier">
                    OR
                </div>
                <div class="socialLogin">
                    <a href="<?php echo base_url(); ?>oauthLogin/fblogin">
                        <img src="<?php echo base_url(); ?>images/fb.png" />
                    </a>
                </div-->
            </div>
        </div>
    </div>
</div>

