<?php $crushers = 0; ?>
<ul class="list-group listPad" style="margin-bottom: 0px !important;">
                              <?php 
                                foreach($trendingVideos as $val): 
                                    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->youtube_url, $match)) {
                                        $vid = $match[1];
                                        $crushers = 1;
                                        $displayedYoutubeVideo = "<iframe width='90' height='80' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                                    }
                                    else {
                                        $displayedYoutubeVideo = $val->youtube_url;
                                    }
                              ?>
    
                              <?php if($crushers == 1){ ?>
                                <a href="<?php echo base_url(); ?>musicians/getfullvideo?mediaset=http://www.youtube.com/embed/<?php echo $vid; ?>" class="majax bhover">
                              <?php } ?>
                                    <li class="list-group-item adjustList bhover">
                                      <span class="badge caser pacer">
                                        <?php echo $val->youtube_title; ?>
                                      </span>
                                      <span class="badge caser ashfont margBot">
                                        By <?php echo $val->uploaded_by; ?>
                                      </span>
                                      <span class="badge caser">
                                              <i class="fa fa-thumbs-up"></i>&nbsp;<?php echo $val->like_count; ?>&nbsp; | &nbsp; 
                                              <i class="fa fa-eye"></i>&nbsp;<?php echo $val->view_count; ?>
                                      </span>
                                      <?php 
                                        if($displayedYoutubeVideo != ''){
                                            if (strpos($val->youtube_url, 'channel') !== false) {
                                      ?>
                                        <div class="vidChannel">
                                            <?php echo anchor($val->youtube_url, 'Visit Channel', array('target' => '_blank', 'class'=>'channelView')); ?>
                                        </div>
                                      <?php
                                            }
                                            else if (strpos($val->youtube_url, 'user') !== false) {
                                      ?>
                                        <div class="vidChannel">
                                            <?php echo anchor($val->youtube_url, 'Visit User', array('target' => '_blank', 'class'=>'channelView')); ?>
                                        </div>
                                      <?php
                                            }
                                            else {
                                                echo $displayedYoutubeVideo;
                                            }
                                        }
                                        else {
                                      ?>
                                        <div class="noVidsShow">
                                            No Video To Display
                                        </div>
                                      <?php
                                        }
                                      ?>
                                    </li>
                                <?php if($crushers == 1){ ?>
                                    </a>
                                <?php } ?>
                              <?php endforeach; ?>
    </ul>
<script type="text/javascript">
    $(document).ready(function(){
        $(".majax").colorbox({width: "85%", top: "-5px"}); 
    });
</script>