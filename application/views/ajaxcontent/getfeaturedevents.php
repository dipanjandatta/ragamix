                 <?php 
                    if(empty($featuredEventsData)){
                ?>
                        <div class="noNots">
                                No Featured Events
                        </div>
                <?php
                    }   
                    else {
                ?>
                <div id="nav-01" class="crsl-nav">
                    <a href="#" class="previous"><i class="fa fa-chevron-left iconArrow"></i></a>
                    <a href="#" class="next"><i class="fa fa-chevron-right iconArrow"></i></a>
		</div>
                    <div class="crsl-items" data-navigation="nav-01">
                        <div class="crsl-wrap">
                            <?php foreach($featuredEventsData as $val): 
                            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->doc_youtube_url, $match)) {
                                $vid = $match[1];
                                $displayedYoutubeVideo = "<iframe width='240' height='225' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                            }    
                            ?>
                            <figure class="crsl-item">
                                <a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$val->category); ?>/<?php echo $val->event_ad_id; ?>">
                                    <div class="col-md-12 noPad stillBord">
                                        <?php
                                            $returnValue = str_replace(' ', '%20', $val->pic_url);
                                            if($val->pic_url != ''){
                                        ?>
                                        <div style="height:230px; max-width: 100%; background-color: #e9ebee; background-position: center 30%; background-size: cover; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
                                            <?php } else { echo $displayedYoutubeVideo; } ?>
                                        <div class="displayNames">
                                            <span class="dps">
                                                <?php echo $val->event_title; ?>
                                            </span>
                                            <span class="kps">
                                                <?php echo $val->event_location; ?>
                                            </span>
                                            <span class="gps">
                                               <?php 
                                                $splitTimeDate = explode(' ',$val->event_date);
                                                echo date('d M Y',  strtotime($splitTimeDate[0])). ' at '. date('H:i A', strtotime($splitTimeDate[1]));
                                               ?>  
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </figure>
                            <?php endforeach; } ?>
                        </div>
                    </div>

<script type="text/javascript">
    $(document).ready(function(){
	$('.crsl-items').carousel({ visible: 4, itemMinWidth: 200, itemMargin: 1 }); 
    });
</script>