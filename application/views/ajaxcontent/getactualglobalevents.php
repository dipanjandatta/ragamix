                        <ul class="list-group listPad">
                         <?php foreach($globalEvents as $val): ?>
                            <a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$val->category); ?>/<?php echo $val->event_ad_id; ?>">
                                <li class="list-group-item adjustList">
                                  <span class="badge caser pacer">
                                    <?php echo $val->event_title; ?>
                                  </span>
                                  <span class="badge caser">
                                    <?php echo $val->event_location; ?>
                                  </span>
                                  <span class="badge caser">
                                    <?php
                                      $splitTimeDate = explode(' ',$val->event_date);
                                      echo date('d M Y', strtotime($splitTimeDate[0])).' @ '.date('h:i A', strtotime($splitTimeDate[1]));
                                    ?>
                                  </span>
                                  <img src="<?php echo $val->pic_url;?>" class="listImgSmall" /> <!-- Replace with $val->pic_url; -->
                                </li>
                            </a>
                          <?php endforeach; ?>
                        </ul>
