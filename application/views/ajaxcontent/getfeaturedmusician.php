                <div id="nav-01" class="crsl-nav">
                    <a href="#" class="previous"><i class="fa fa-chevron-left iconArrow"></i></a>
                    <a href="#" class="next"><i class="fa fa-chevron-right iconArrow"></i></a>
		</div>
                    <div class="crsl-items" data-navigation="nav-01">
                        <div class="crsl-wrap">
                            <?php foreach($featuredMusicianData as $val): ?>
                            <figure class="crsl-item">
                                <a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>">
                                    <div class="col-md-12 noPad stillBord">
                                        <?php
                                            $returnValue = str_replace(' ', '%20', $val->profile_pic_url);
                                        ?>
                                        <div style="height:230px; max-width: 100%; background-color: #e9ebee; background-position: center 30%; background-size: cover; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
                                        <div class="displayNames">
                                            <span class="dps">
                                                <?php echo $val->user_name; ?>
                                            </span>
                                            <span class="gps">
                                                <?php if($val->genre){ 
                                                        echo "Genre: " .$val->genre;
                                                    } 
                                                ?>
                                            </span>
                                            <span class="kps">
                                                <?php echo $val->city_name; ?>
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </figure>
                            <?php endforeach; ?>
                        </div>
                    </div>

<script type="text/javascript">
    $(document).ready(function(){
	$('.crsl-items').carousel({ visible: 4, itemMinWidth: 200, itemMargin: 1 }); 
    });
</script>