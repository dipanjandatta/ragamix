                    <?php 
                        foreach($spotNews as $val): 
                            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->doc_youtube_url, $match)) {
                                $vid = $match[1];
                                $displayedYoutubeVideo = "<iframe width='240' height='165' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                            }
                    ?>
                    <a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$val->category); ?>/<?php echo $val->event_ad_id; ?>">
                        <div class="col-md-3 noPad stillBord">
                            <div class="demo-section k-content stillHeight">
                                <div class="coverClass blurim">
                                    <?php if($val->doc_youtube_url != '') {
                                    	echo $displayedYoutubeVideo;
                                    } else { ?> 
                                        <img src="<?php echo $val->pic_url;?>" class="newImg" />
                                    <?php
                                    }    
                                    ?>
                                </div>
                                <div class="title grey">
                                    <?php echo $val->event_title; ?>
                                </div>
                                <div class="stnComm greyblack">
                                  <?php
                                        if(strlen($val->event_desc) > 100) {
                                        // truncate string
                                         $stringCut = substr($val->event_desc, 0, 100);

                                         // make sure it ends in a word so assassinate doesn't become ass...
                                         $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
                                        }
                                        else {
                                            $string = $val->event_desc;
                                        }
                                        echo $string; 
                                  ?>
                                </div>
                            </div>
                        </div>
                    </a>
                    <?php endforeach; ?>