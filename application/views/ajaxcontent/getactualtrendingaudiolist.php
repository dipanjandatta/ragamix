<ul class="list-group listPad" style="margin-bottom: 0px !important;">
                              <?php 
                                foreach($trendingAudio as $val): 
                              ?>
                                
                                    <li class="list-group-item adjustList bhover">
                                      <span class="badge caser pacer">
                                        <?php echo $val->audio_title; ?>
                                      </span>
                                      <span class="badge caser ashfont margBot">
                                        By <?php echo $val->uploaded_by; ?>
                                      </span>
                                      <span class="badge caser">
                                              <i class="fa fa-file"></i>&nbsp;
                                                  <?php $kbSize = (($val->size)/1024);
                                                        $mbSize = $kbSize/1024;
                                                        echo round($mbSize,2)." Mb"; ?>
                                              
                                      </span>
                                        <span class="playopt">
                                            <a href = "javascript:void(0)" onclick = "document.getElementById('player<?php echo $val->serial_no; ?>').play();document.getElementById('light<?php echo $val->serial_no; ?>').style.display='block';document.getElementById('fade<?php echo $val->serial_no; ?>').style.display='block'"><i class="fa fa-play-circle"></i></a>
                                        </span>
                                        <i class="fa fa-music" style="font-size: 45px; padding: 10px;"></i>
                                        <div id="light<?php echo $val->serial_no; ?>" class="white_content">
                                            <audio class="fixAudioWidth" id="player<?php echo $val->serial_no; ?>" controls>
                                              <source src="<?php echo $val->audio_url; ?>" type="audio/mpeg">
                                              Your browser does not support the audio element.
                                            </audio>
                                        </div>
                                    </li>
                                
                              <?php endforeach; ?>
    </ul>
