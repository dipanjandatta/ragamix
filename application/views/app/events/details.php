<?php
    $splitTimeDate = explode(' ',$getParticularEvent[0]->event_date);
    $evdate = date('d M Y', strtotime($splitTimeDate[0]));
    $evTime = date('H:i A', strtotime($splitTimeDate[1]));
    $returnValue = str_replace(' ', '%20', $getParticularEvent[0]->pic_url);
    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $getParticularEvent[0]->doc_youtube_url, $match)) {
        $vid = $match[1];
        $displayedYoutubeVideo = "<iframe width='600' height='400' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
    }
?>
<div class="container stanPad">
    <div class="row margTop">
        <div class="col-md-12 noPad">
            <div class="col-md-6 noPad">
                <div class="col-md-12 noPad">
                    <!-- Works well with Most of the images but vertical . If wrong go back to 3 instead of 6-->
                    <?php if($getParticularEvent[0]->doc_youtube_url != '') {
                    	echo $displayedYoutubeVideo; 
                    
                    } else { ?>
                    	<div style="height:400px; max-width: 100%; background-size: cover; background-position: center 50%; background-color: #e9ebee; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
					<?php                        
                    }
                    ?>
                    <div class="eventsGrasps">
                        <div class="eventTitleGem">
                            <i class="fa fa-calendar"></i> <?php echo $evdate; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 noPad margTop">
                        <button class="lightMaroon" id="notserp">Share This Event</button>
                    <div id="notificationsp">
                        <span class="shareHead">Share on Social Media</span>
                        <div class="agyat">
                            <div class="fb-share-button" data-href="<?php echo current_url(); ?>" data-layout="button" data-mobile-iframe="true"></div>
                        </div>
                        <div class="agyat">
                            <div class="g-plus" data-action="share" data-annotation="none" data-href="<?php echo current_url(); ?>"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="catShow">
                        Category: <?php echo $getParticularEvent[0]->category; ?>
                    </div>
                    <div class="evnameNormal">
                        <?php echo $getParticularEvent[0]->event_title; ?>
                    </div>
                    <div class="panel panel-default noBord">
                        <div class="panel-body panFan banban">
                            <div class="col-md-12 noPad smallTicksBord uniPadTwo">
                                <i class="fa fa-calendar"></i>&nbsp;<?php echo $evdate; ?> | <?php echo $evTime; ?>
                            </div>
                            <div class="col-md-12 noPad smallTicksBord uniPadTwo">
                                <i class="fa fa-map-marker"></i>&nbsp;<?php echo $getParticularEvent[0]->event_location; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 margTop">
                    <?php echo $map['html']; ?>
                </div>
            </div>
            <div class="col-md-12 noPad margTop fontMed">
                <div class="col-md-6 noPad">
                    Event Details
                </div>
                <div class="col-md-12 noPad margTop">
                    <span class="eventSimpleTextSub">
                        <?php echo $getParticularEvent[0]->event_desc; ?>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-12 noPad bigMargTop">
            <?php if($cat == 2) { ?>
            <div class="panel panel-default noBord">
                <div class="panel-heading panelSubMain seamseam">
                    Reviews you might be interested in
                </div>
                    <?php foreach($reviews as $val): ?>
                    <a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$val->category); ?>/<?php echo $val->event_ad_id; ?>">
                        <div class="col-md-2-3 noPad stillBord">
                            <div class="demo-section k-content stillHeight">
                                <div class="coverClass blurim">
                                    <img src="<?php echo $val->pic_url;?>" class="newImg" />
                                </div>
                                <div class="title grey">
                                    <?php echo $val->event_title; ?>
                                </div>
                                <div class="stnComm greyblack">
                                  <?php
                                        if(strlen($val->event_desc) > 100) {
                                        // truncate string
                                         $stringCut = substr($val->event_desc, 0, 100);

                                         // make sure it ends in a word so assassinate doesn't become ass...
                                         $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
                                        }
                                        else {
                                            $string = $val->event_desc;
                                        }
                                        echo $string; 
                                  ?>
                                </div>
                            </div>
                        </div>
                    </a>
                    <?php endforeach; ?>
                </div>
            <?php } if($cat == 1) { ?>
            <div class="panel panel-default noBord">
                <div class="panel-heading panelSubMain seamseam">
                    NewBits you might be interested in
                </div>
                    <?php 
                        foreach($spotNews as $val): 
                        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $val->doc_youtube_url, $match)) {
                            $vid = $match[1];
                            $displayedYoutubeVideo = "<iframe width='260' height='165' src='http://www.youtube.com/embed/$vid ' allowfullscreen></iframe>";
                        }
                    ?>
                    <a href="<?php echo base_url(); ?>events/detail/<?php echo str_replace(' ','',$val->category); ?>/<?php echo $val->event_ad_id; ?>">
                        <div class="col-md-2-3 noPad stillBord">
                            <div class="demo-section k-content stillHeight">
                                <div class="coverClass blurim">
                                    <?php if($val->pic_url != '') { ?>
                                        <img src="<?php echo $val->pic_url;?>" class="newImg" />
                                    <?php } else { 
                                        echo $displayedYoutubeVideo;
                                    }
                                    ?>
                                </div>
                                <div class="title grey">
                                    <?php echo $val->event_title; ?>
                                </div>
                                <div class="stnComm greyblack">
                                  <?php
                                        if(strlen($val->event_desc) > 100) {
                                        // truncate string
                                         $stringCut = substr($val->event_desc, 0, 100);

                                         // make sure it ends in a word so assassinate doesn't become ass...
                                         $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
                                        }
                                        else {
                                            $string = $val->event_desc;
                                        }
                                        echo $string; 
                                  ?>
                                </div>
                            </div>
                        </div>
                    </a>
                    <?php endforeach; ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>