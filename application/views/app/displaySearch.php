<div class="dispSearchLabel">
    Profiles
</div>
<?php foreach($searchListM as $val): ?>
<a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>">
    <div class="display_box">
        <div class="coverClass searchProfImgHolder">
            <img src="<?php echo $val->profile_pic_url; ?>" class="pofimg" />
        </div>
        <div class="placehold">
            <?php echo $val->user_name; ?>
            <span class="otlabtext">
                <?php echo $val->user_category; ?>, <?php echo $val->city_name; ?>
            </span>
        </div>

    </div>
</a>
<?php endforeach; ?>
<div class="divide"></div>
<!--<div class="dispSearchLabel">
    Singers
</div>
<?php foreach($searchListS as $val): ?>
<a href="<?php echo base_url(); ?>profile/details/musician/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>">
    <div class="display_box">  
        <div class="coverClass">
            <img src="<?php echo $val->profile_pic_url; ?>" class="pofimg" />
        </div>
        <div class="placehold">
            <?php echo $val->user_name; ?>
            <span class="otlabtext">
                <?php echo $val->user_category; ?>
            </span>
            <span class="otlabtext">
                Lives in <?php echo $val->city_name; ?>
            </span>
        </div>

    </div>
</a>
<?php endforeach; ?>
<div class="divide"></div>
<div class="dispSearchLabel">
    Bands
</div>
<?php foreach($searchListB as $val): ?>
<a href="<?php echo base_url(); ?>profile/details/band/<?php echo $val->user_name; ?>/<?php echo $val->rm_id; ?>">
    <div class="display_box">  
        <div class="coverClass">
            <img src="<?php echo $val->profile_pic_url; ?>" class="pofimg" />
        </div>
        <div class="placehold">
            <?php echo $val->user_name; ?>
            <span class="otlabtext">
                <?php echo $val->user_category; ?>
            </span>
            <span class="otlabtext">
                Lives in <?php echo $val->city_name; ?>
            </span>
        </div>

    </div>
</a>
<?php endforeach; ?>
<div class="divide"></div>-->
<?php echo anchor('profile/searchTop?q='.$sv.'&city='.$city.'&ref=all&type=oo', '<div class="profiler"><span class="iconer"><i class="fa fa-search fafix"></i></span><span class="texter">See all results for "'.$sv.'"</span></div>'); ?>