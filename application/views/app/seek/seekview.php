<div class="container stanPad">
    <div class="row">
        <div class="col-md-12 noPad">
            <div class="col-md-9  noPad">
                <div class="col-md-12">
                    <div class="hori"></div>
                    <div class="pom">
                        Jobs & Opportunities <span class="cr"><?php echo $mc; ?></span>
                    </div>
                    <div class="horir"></div>
                    <div class="pagingholder">
                        <form id="live-search" action="" class="styledLeft" method="post">
                            <input type="text" class="text-input bigInput ragacontrols" id="filter" value="" placeholder="Filter From the current List" />
                            <span id="filter-count"></span>
                        </form>
                        <?php if($page_number != 0){ ?>
                            <a href="<?php echo base_url(); ?>seek/view?next=<?php echo $next; ?>&clicks=<?php echo ($clicks-1); ?>&page_number=<?php echo ($page_number-1); ?>&prev=y">
                                <i class="fa fa-chevron-left pageicon"></i>
                            </a>
                        <?php } ?>
                        <a href="<?php echo base_url(); ?>seek/view?next=<?php echo $next; ?>&clicks=<?php echo $clicks; ?>&page_number=<?php echo $page_number; ?>">
                            <i class="fa fa-chevron-right pageicon"></i>
                        </a>
                    </div>
                    <div class="jewel">
                        <?php
                        if(empty($seeklist)){
                            ?>
                            <div class="noNots">
                                No More Results to Show
                            </div>
                            <?php
                        }
                        else {
                            foreach($seeklist as $val):
                                $returnValue = str_replace(' ', '%20', $val->profile_pic_url);
                                ?>
                            <a href="<?php echo base_url(); ?>seek/seekdetails/<?php echo $val->serial_no; ?>">
                                <div class="col-md-6 noPad colerBackMg commentList">
                                <div class="display_box ser">
                                <div class="coverClass searchProfImgHolderBig">
                                    <div style="height:90px; max-width: 100%; background-size: cover; background-position: center 50%; background-color: #e9ebee; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValue; ?>);"></div>
                                </div>
                                <div class="placeholdLeft">

                                    <span style="font-weight: bold;font-size: 13px;"><?php if(strlen($val->keyword) > 20){ echo substr($val->keyword, 0, 20)."...";}else{ echo substr($val->keyword, 0, 20); } ?></span><br><span style=" font-size: 12px;"><?php if(strlen($val->location) > 20){ echo substr($val->location, 0, 20)."..."; }else{  echo substr($val->location, 0, 20); } ?></span>
                                    <span class="otlabtext" style="color: #3a1b1b !important;font-size: 12px !important;">
                                    <?php if(strlen($val->description) > 40){ echo substr($val->description, 0, 40)."...";}else{ echo substr($val->description, 0, 40); } ?><br><br>
                                     <span style="font-size: 10px;">Posted On:<?php echo date('d M Y', strtotime($val->updated_on)); ?></span>
                                </span>
                                <span class="otlabtext">
                                    <?php if($val->full_name != '') {
                                        echo "Full Name ".$val->full_name;
                                    }
                                    ?>
                                </span>
                                <span class="otlabtext">
                                    <?php echo $val->genre; ?>
                                </span>
                                </div>
                                <?php if($this->session->userdata('user_type_id') == 3) {
//                                                echo anchor('profile/profileEdit/basic/'.$val->rm_id, '<i class="fa fa-edit"></i>');
//                                                echo anchor('admin/profileList/delete/'.$val->rm_id, '<i class="fa fa-close"></i>');
                                } ?>
                                </div>
                                </div>
                                </a>
                                <?php endforeach;} ?>

                    </div>
                </div>
            </div>
            <div class="col-md-3" >
      <?php
      $adsence = "
<div class=\"right-inner\">
            <center width=\"96% class=\"img-responsive center-block\">
                            <script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script>

<!-- quiz_net -->
              <ins class=\"adsbygoogle\"
                 style=\"display:block\"
                 data-ad-client=\"$client\"
                 data-ad-slot=\"$slot\"
                 data-ad-format=\"auto\"></ins>
              <script>
              (adsbygoogle = window.adsbygoogle || []).push({});
              </script>
            </center>
</div>";

      echo $adsence;
      ?>
            </div>
 <div class="col-md-3" >
                <div class="col-md-12 noPad">
                    <div class="panel panel-default noBord">
                        <div class="panel-heading panelMaroon">
                           Ads
                        </div>
                        <div class="panel-body panFormer banban">

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
