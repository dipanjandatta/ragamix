                <div id="ei-slider" class="ei-slider">
                    <ul class="ei-slider-large">
						<li>
                            <img src="http://cdn.ragamix.com/home2.jpg" alt="image06"/>
                            <div class="ei-title">
                                <h2 class="homePageCaption">
                                    When word fails, music speaks<br />
                                    <span class="yelcolfont" style="font-size: 33px;">Bond and Band over Ragamix</span>
                                </h2>
                                <!--<h3>Seeker</h3>-->
                            </div>
                        </li>
                        <li>
                            <img src="http://cdn.ragamix.com/home3.jpg" alt="image01" />
                            <div class="ei-title">
                                <h2 class="homePageCaption">
                                    Nothing gives a better feeling then playing guitar<br />
                                    <span class="yelcolfont" style="font-size: 33px;">Join us today, FREE</span>
                                </h2>
                            </div>
                        </li>
                        <li>
                            <img src="http://cdn.ragamix.com/home4.jpg" alt="image02" />
                            <div class="ei-title">
                                <h2 class="homePageCaption">
                                    Play Black &amp; White notes together<br />
                                    <span class="yelcolfont" style="font-size: 33px;">Ripple million colors on your mind</span>
                                </h2>
                            </div>
                        </li>
                        <li>
                            <img src="http://cdn.ragamix.com/home5.jpg" alt="image03"/>
                            <div class="ei-title">
                                <h2 class="homePageCaption">
                                    Never settle for less<br />
                                    <span class="yelcolfont" style="font-size: 33px;">Give wings to your dreams through Ragamix</span>
                                </h2>
                            </div>
                        </li>
                        <li>
                            <img src="http://cdn.ragamix.com/home1.jpg" alt="image03"/>
                            <div class="ei-title">
                                <h2 class="homePageCaption">
                                    If you never try, you will never know<br />
                                    <span class="yelcolfont" style="font-size: 33px;">Unfold the potentials </span>
                                </h2>
                            </div>
                        </li>
                        <li>
                            <img src="http://cdn.ragamix.com/home6.jpg" alt="image03"/>
                            <div class="ei-title">
                                <h2 class="homePageCaption">
                                    Life is song, Love is the music<br />
                                    <span class="yelcolfont" style="font-size: 33px;">Let's play on Ragamix</span>
                                </h2>
                            </div>
                        </li>
                    </ul><!-- ei-slider-large -->
                    <ul class="ei-slider-thumbs">
                        <li class="ei-slider-element">Current</li>
                        <li><a href="#">Slide 1</a><img src="<?php echo base_url(); ?>images/home2.jpg" alt="thumb06" /></li>
                        <li><a href="#">Slide 2</a><img src="<?php echo base_url(); ?>images/home3.jpg" alt="thumb01" /></li>
                        <li><a href="#">Slide 3</a><img src="<?php echo base_url(); ?>images/home4.jpg" alt="thumb02" /></li>
                        <li><a href="#">Slide 4</a><img src="<?php echo base_url(); ?>images/home5.jpg" alt="thumb03" /></li>
                        <li><a href="#">Slide 5</a><img src="<?php echo base_url(); ?>images/home1.jpg" alt="thumb03" /></li>
                        <li><a href="#">Slide 6</a><img src="<?php echo base_url(); ?>images/home6.jpg" alt="thumb03" /></li>
                    </ul><!-- ei-slider-thumbs -->
                </div><!-- ei-slider -->
<div class="container stanPadSmall">
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-3-4 noPad">
        <div class="col-md-12">
        <div class="panel panel-default noBord">
          <div class="panel-heading panelSubMain">
            EVENTS
          </div>
          <div class="panel-body panFan banban">
              <div id="homepageeventslist"></div>
        </div>
      </div>
    </div>
        <div class="col-md-12">
          <div class="panel panel-default noBord">
            <div class="panel-heading panelSubMain">
              TOP SPOTS
            </div>
            <div class="panel-body panFan banban">
                <div id="topspots"></div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="panel panel-default noBord">
            <div class="panel-heading panelSubMain">
              LATEST ADS
            </div>
            <div class="panel-body panFan banban">
                <div id="getlatestadshomepage"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-9-4 noPad">
        <!--<div class="demo-section k-content">-->
          <div class="panel panel-default noBord">
            <div class="panel-heading panelSubMain colHome">
                <span class="hname">
                    News
                </span>
                <span class="hopt">
                    <a id="hideHomeOne" class="fc">
                        Show/Hide
                    </a>
                </span>
            </div>
              <div class="panel-body panFan kankan">
                <div class="col-md-12 nbts">
                    <div id="newsbitshomepage"></div>
                </div>
              </div>
          </div>
          <div class="panel panel-default noBord">
            <div class="panel-heading panelSubMain colHome">
                <span class="hname">
                    Featured Profiles
                </span>
                <span class="hopt">
                    <a id="hideHomeTwo" class="fc">
                        Show/Hide
                    </a>
                </span>
            </div>
              <div class="panel-body panFan kankan">
                <div class="col-md-12 fps">
                    <div id="fphome"></div>
                </div>
              </div>
          </div>
          <div class="panel panel-default noBord">
            <div class="panel-heading panelSubMain colHome">
                <span class="hname">
                    Reviews
                </span>
                <span class="hopt">
                    <a id="hideHomeThree" class="fc">
                        Show/Hide
                    </a>
                </span>
            </div>
              <div class="panel-body panFan kankan">
                <div class="col-md-12 rhp">
                    <div id="getreviewshomepage"></div>
                </div>
              </div>
          </div>
          <div class="panel panel-default noBord">
            <div class="panel-heading panelSubMain colHome">
                <span class="hname">
                    Interviews
                </span>
                <span class="hopt">
                    <a id="hideHomeFour" class="fc">
                        Show/Hide
                    </a>
                </span>
            </div>
              <div class="panel-body panFan kankan">
                <div class="col-md-12 rhp">
                    <div id="getinterviewshomepage"></div>
                </div>
              </div>
          </div>
        <!--</div>-->
      </div>
  </div>
</div>
  </div>
<?php if($this->uri->segment(3) == 'roadBlock'){ ?>
<div class="roadBlockUILayer">
    <div class="roleDialog">
        <div class="actDialog">
            <div class="dialogHead">
                Complete The Process...
            </div>
            
                <?php echo form_open('oauthLogin/roadblockcomplete'); ?>
                <div class="uiLay">
                        <div class="control-group form-group ragaformarFix" style="margin-bottom: 15px !important;">
                          <div class="controls">
                              <input type="text" id="user_name_in" name="user_name_in" class="form-control ragacontrols textFormControls" placeholder="Provide an User Name">
                              <input type="hidden"  name="email_in" value="<?php echo $this->uri->segment(4); ?>" class="form-control ragacontrols textFormControls" >
                              <input type="hidden"  name="facebook_id_in" value="<?php echo $this->uri->segment(5); ?>" class="form-control ragacontrols textFormControls" >
                              <span id="status"></span>
                          </div>
                        </div>
                </div>
                <div class="uiOverlayFooter">
                    <input type="submit" name="submit" value="Finish" class="btn btnfix greenbtn" style="margin-bottom: 3px; margin-top: 3px;" />
                </div>
                <?php echo form_close(); ?>
        </div>
    </div>
</div>
<?php } ?>
