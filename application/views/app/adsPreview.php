        <div class="col-md-12 uniPad">
                        <div class="table-responsive">  
                        <?php if(empty($getAds)) { ?>
                            <div class="noNots">
                                No Ads Posted
                            </div>
                        <?php } else { ?>
                        <table class="table table-striped tabmarun">
                          <thead>
                            <tr>
                                <th class="wf">Posted Ads</th>
                                <th class="wf">Status</th>
                                <th class="wf">Ad Title</th>
                                <th class="wf">Views</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="width: 30%;"><?php echo date('d M Y',strtotime($getAds[0]->created_on)); ?> <img src="<?php echo base_url(); ?>adsimage/<?php echo $getAds[0]->photo_id; ?>" style="max-width: 60%;" /></td>
                              <td>Active</td>
                              <td><?php echo $getAds[0]->ads_desc; ?></td>
                              <td><?php echo $getAds[0]->ads_views; ?></td> 
                            </tr>
                          </tbody>
                        </table>
                        <?php } ?>
                        </div>
        </div>