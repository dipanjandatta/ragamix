<div class="container stanPad">
        <div class="col-md-12 noPad whiteBg negMargin">
            <div class="profileFixedDisplay">
                <div class="col-md-12 profileCol" style="padding: 20px;">
                    <div class="col-md-3">
                        <div style="height:220px; max-width: 220px; background-size: cover; background-color: #e9ebee; background-position: center 30%; background-repeat: no-repeat; display: block;background-image: url('<?php echo $previewData[0]->profile_pic_url; ?>'); border-radius: 50%;"></div>
                    </div>
                    <div class="col-md-5 noPad">
                        <div class="profileDetsMin">
                            <?php echo $previewData[0]->user_name; ?>
                        </div><br />
                        <div class="profileDetsOts ashCol">
                            <?php echo $previewData[0]->usertypename; ?>, <?php echo $previewData[0]->genre_name; ?>
                        </div>
                        <div class="profileDetsOts yelcolfont">
                            <?php echo $previewData[0]->location_info; ?>
                        </div>
                        <div class="montevera">
                            <ul class="profileUlist">
                                <?php if($previewData[0]->facebook_url != '')
                                    { 
                                        if(strpos($previewData[0]->facebook_url,'facebook') !== false) {
                                ?>
                                <li class="profileList">
                                    <a href="<?php echo $previewData[0]->facebook_url; ?>" class="uibutton large confirm" target="_blank">
                                        Find on Facebook
                                    </a>
                                </li>
                                <?php }
                                } if($previewData[0]->twitter_id != '') { ?>
                                <li class="profileList">
                                    <a href="<?php echo $previewData[0]->twitter_url; ?>" class="uibutton large tweet" target="_blank">
                                        Find on Twitter
                                    </a>
                                </li>
                                <?php } ?>
                                <li class="profileList">
                                    <a data-toggle="modal" data-target="#sendmessage" class="uibutton large special">
                                        Send <?php echo $previewData[0]->user_name; ?> a message
                                    </a>

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 text-right">
                        <div class="fb-share-button" data-href="<?php echo current_url(); ?>" data-layout="button" data-mobile-iframe="true">
                            Share Profile in Facebook
                        </div><br /><br />
                        <div class="g-plus" data-action="share" data-annotation="none" data-href="<?php echo current_url(); ?>"></div>
                    </div>
                                <div class="modal fade" id="sendmessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h1 class="modal-title">Send a Message</h1>
                                      </div>

                                      <div class="modal-body">
                                          <?php 
                                            if($this->session->userdata('user_id')) {
                                            echo form_open('profile/sendmessagefromprofile'); 
                                          ?>
                                            <textarea name="body_text" cols="38" rows="10" placeholder="Create a message"></textarea>
                                            <input type="hidden" name="toid" value="<?php echo $previewData[0]->rm_id; ?>" />
                                            <input type="submit" name="submit" value="Post Message" class="uibutton large" />
                                          <?php 
                                            echo form_close(); 
                                            }
                                            else {
                                                echo "Please Login to Send a message to ".$previewData[0]->user_name;
                                            }
                                          ?>
                                      </div>
                                      <div class="modal-footer"> 
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-4 noPad" style="border-right: 1px solid #eaeaea;">
<!--                    <ul class="semiList">
                        <li class="sListing">
                            <span class="fmtLabel">
                                Joined Since
                            </span>
                            <span class="fmtText">
                                <?php echo date('d M Y', strtotime($previewData[0]->signup_date)); ?>
                            </span>
                        </li>
                        <li class="sListing">
                            <span class="fmtLabel">
                                Experience
                            </span>
                            <span class="fmtText">
                                <?php echo $previewData[0]->experience_in_years; ?>
                            </span>
                        </li>
                    </ul>-->
                    <div class="col-md-12 padfteen">
                        <span class="profileAText">
                            About <?php echo $previewData[0]->user_name; ?>
                        </span>
                    </div>
                    <div class="col-md-12 textjustify">
                        <span class="profileWhole">
                        <?php 
                            if(strlen($previewData[0]->user_desc) > 400) {
                            // truncate string
                             $stringCut = substr($previewData[0]->user_desc, 0, 400);

                             // make sure it ends in a word so assassinate doesn't become ass...
                             $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a data-toggle="modal" data-target="#seemore">Read More</a>'; 
                            }
                            else {
                                $string = $previewData[0]->user_desc;
                            }
                            echo nl2br($string); 
                        ?>
                        </span>
                    </div>
                    <div class="modal fade" id="seemore" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h1 class="modal-title">About <?php echo $previewData[0]->user_name; ?></h1>
                          </div>
                          
                          <div class="modal-body">
                              <?php echo nl2br($previewData[0]->user_desc); ?>
                          </div>
                          <div class="modal-footer"> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12 padfteen">
                        <div class="col-md-12 noPad bordertopgrey">
                            <div class="col-md-3 noPad text-right">
                                <span class="otrdetsprofnew">
                                    Music Preference
                                </span>
                            </div>
                            <div class="col-md-9">
                                <span class="tt">
                                    <?php echo $previewData[0]->music_influence_desc; ?>
                                </span>
                            </div>
                            <div class="col-md-3 noPad text-right">
                                <span class="otrdetsprofnew">
                                    Musical Atts
                                </span>
                            </div>
                            <div class="col-md-9">
                                <span class="tt">
                                    <?php if($previewData[0]->freelance_professional != '') { echo $previewData[0]->freelance_professional."<br />";} ?>
                                    <?php if($previewData[0]->studio_band_member != '') { echo $previewData[0]->studio_band_member."<br />"; } ?>
                                    <?php if($previewData[0]->commitment_lvl != '') { echo $previewData[0]->commitment_lvl."<br />"; } ?>
                                    <?php if($previewData[0]->equipment_owned != '') { echo $previewData[0]->equipment_owned; } ?>
                                </span>
                            </div>
                            <?php if($previewData[0]->seeking_description != '') { ?>
                            <div class="col-md-3 noPad text-right">
                                <span class="otrdetsprofnew">
                                    Seeking
                                </span>
                            </div>
                            <div class="col-md-9">
                                <span class="tt">
                                    <?php echo $previewData[0]->seeking_description; ?>
                                </span>
                            </div>
                            <?php } ?>
                            <?php if($previewData[0]->educator_type != '') { ?>
                            <div class="col-md-3 noPad text-right">
                                <span class="otrdetsprofnew">
                                    Teaching Details
                                </span>
                            </div>
                            <div class="col-md-9">
                                <span class="tt">
                                    <?php echo $previewData[0]->educator_type; ?><br />
                                    <?php echo $previewData[0]->educator_desc; ?>
                                </span>
                            </div>
                            <?php } ?>
                            <?php if($previewData[0]->band_type != '') { ?>
                            <div class="col-md-3 noPad text-right">
                                <span class="otrdetsprofnew">
                                    Teaching Details
                                </span>
                            </div>
                            <div class="col-md-9">
                                <span class="tt">
                                    <?php echo $previewData[0]->band_type; ?><br />
                                    <?php echo $previewData[0]->band_desc; ?>
                                </span>
                            </div>
                            <?php } ?>
                            <div class="col-md-12">
                                <?php echo $map['html']; ?>
                            </div>
                        </div>
                    </div>
                </div>                
                <div class="col-md-8" style="padding-top: 10px;">
                    <ul id="countrytabs" class="shadetabs text-right" style="text-align: right !important;">
                        <li><a href="<?php echo base_url(); ?>profile/sa/<?php echo $previewData[0]->rm_id; ?>" rel="countrycontainer" class="selected">Songs &amp; Albums</a></li>
                        <li><a href="<?php echo base_url(); ?>profile/pg/<?php echo $previewData[0]->rm_id; ?>" rel="countrycontainer">Photo Gallery</a></li>
                        <li><a href="<?php echo base_url(); ?>profile/se/<?php echo $previewData[0]->rm_id; ?>" rel="countrycontainer">Shows &amp; Events</a></li>
                        <li><a href="<?php echo base_url(); ?>profile/ads/<?php echo $previewData[0]->rm_id; ?>" rel="countrycontainer">Ads</a></li>
                    </ul>
                    <div id="countrydivcontainer" style="margin-top: 6px;border-bottom: 1px solid #f2f2f2;"></div>
                </div>
            </div>
        </div>
    
</div>
