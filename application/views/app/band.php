<?php
$returnValueBack = str_replace(' ', '%20', $previewData[0]->background_pic_url);
$returnValueProf = str_replace(' ', '%20', $previewData[0]->profile_pic_url);
?>
<div class="container">
    <div class="row" style="margin-top: 20px;">
        <div class="col-md-12 noPad">
            <div id="timelineContainer">
                <div id="timelineBackground">
                    <?php if($previewData[0]->background_pic_url == ''){ ?>
                        <!--<img src="<?php echo $previewData[0]->profile_pic_url; ?>" id="timelineBGload" class="headerimage" style="top: -40px;">-->
                        <div style="height:400px; max-width: 100%; background-size: cover; background-position: center 25%; background-color: #e9ebee; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValueProf; ?>);"></div>
                    <?php } else { ?>
                        <!--<img src="<?php echo $previewData[0]->background_pic_url; ?>" id="timelineBGload" class="headerimage" style="top: -40px;">-->
                        <div style="height:400px; max-width: 100%; background-size: cover; background-position: center 25%; background-color: #e9ebee; background-repeat: no-repeat; display: block; background-image: url(<?php echo $returnValueBack; ?>);"></div>
                    <?php } ?>
                </div>
                <div id="timelineProfilePic">
                    <img src="<?php echo $previewData[0]->profile_pic_url; ?>" id="timelineBGload" />
                </div>
                <div id="timelineTitle">
                    <span class="rolename">
                        <?php echo $previewData[0]->user_name; ?>
                    </span>
                    <span class="roletype">
                        <?php echo $previewData[0]->genre_name; ?> 
                    </span>
                    <span class="rolereside">
                        <?php echo $previewData[0]->city_name; ?>
                    </span>
                    <span class="rolesocials">
                        <?php if($previewData[0]->facebook_url != '') { ?>
                        <a href="<?php echo $previewData[0]->facebook_url; ?>" target="_blank"><i class="fa fa-facebook-square"></i></a>&nbsp;<?php } ?>
                        <?php if($previewData[0]->twitter_id != '') { ?>
                        <a href="<?php echo $previewData[0]->twitter_id; ?>"><i class="fa fa-twitter-square"></i></a>&nbsp;<?php } ?>
                        <?php if($previewData[0]->gmail_id != '') { ?>
                        <a href="<?php echo $previewData[0]->gmail_id; ?>"><i class="fa fa-google-plus-square"></i>&nbsp;<?php } ?>
                        <i class="fa fa-envelope-square"></i>&nbsp;
                        <i class="fa fa-share-alt" id="notserp" style="cursor: pointer"></i>
                    </span>
                    <div id="notificationsp">
                        <span class="shareHead">Share on Social Media</span>
                        <div class="agyat">
                            <div class="fb-share-button" data-href="<?php echo current_url(); ?>" data-layout="button" data-mobile-iframe="true"></div>
                        </div>
                        <div class="agyat">
                            <div class="g-plus" data-action="share" data-annotation="none" data-href="<?php echo current_url(); ?>"></div>
                        </div>
                    </div>
                </div>
                <div class="otherDets">
                    <!--Facebook <i class="fa fa-thumbs-up"></i>:<?php echo $haha[0]->like_count; ?>-->
                </div>
            </div>

        </div>
    </div>
        <div class="row" style="margin-top: 20px;">
        <div class="col-md-12">
            <ul id="countrytabs" class="shadetabs">
                <li><a href="<?php echo base_url(); ?>band/bsa/<?php echo $previewData[0]->rm_id; ?>" rel="countrycontainer" class="">Songs &amp; Videos</a></li>
                <li><a href="<?php echo base_url(); ?>band/bpg/<?php echo $previewData[0]->rm_id; ?>" rel="countrycontainer" class="">Photo Gallery</a></li>
                <li><a href="<?php echo base_url(); ?>band/ba/<?php echo $previewData[0]->rm_id; ?>" rel="countrycontainer" class="selected">Profile</a></li>
                <li><a href="<?php echo base_url(); ?>band/bst/<?php echo $previewData[0]->rm_id; ?>" rel="countrycontainer" class="">Shows &amp; Tours</a></li>
            </ul>
                    <div id="countrydivcontainer" style="margin-top:  20px;">

                    </div>
        </div>
        </div>
</div>
