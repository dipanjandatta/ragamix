<div class="col-md-12 uniPad">
    <div class="cont list">
        <div class="cardRow">
                                <?php if(empty($getEvents)) { ?>
                                    <div class="noNots">
                                        No Shows / Events To List
                                    </div>
                                <?php } else { 
                                    foreach($getEvents as $val):
                                ?>
                                <div class="card">
                                    <div class="title">
                                        <h2 class="ng-binding"><?php echo $val->event_title; ?>@<?php echo $val->event_location; ?></h2>
                                        <!--<span class="ng-binding">Following: 21,456</span>-->
                                    </div>
                                    <div class="title">
                                        <h2 class="ng-binding">
                                            <?php echo date('d M Y', strtotime($val->event_date)); ?>
                                        </h2>
                                    </div>
                                    <div class="title">
                                        <h2 class="ng-binding"><?php echo $val->event_location; ?></h2>
                                        <span class="ng-binding"><?php echo $val->event_city; ?></span>
                                    </div>
                                    <div class="title">
                                        <?php if($val->event_approved == 1) { ?>
                                        <h2 class="ng-binding">Approved</h2>
                                        <?php } else { ?>
                                        <h2 class="ng-binding">Pending</h2>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php endforeach; } ?>
        </div>
    </div>
</div>