<div class="container">
    <div class="col-md-12 noPad">
        <div class="col-md-4" id="leftcolumn"></div>
        <?php if($this->uri->segment(3) == ''){ ?>
        <div class="col-md-5 noPad rightBorder" id="rightcolumn">
            <div class="col-md-12 whiteBg noPad">
            <div class="audacity">
                New Message
            </div>
                <form method="post" name="form" action="">
                    <input type="hidden" id="courseboxid" />
                    <div class="msgsendcontainer">
                        <span class="tomsg">To:</span><input type="text" class="course msgtextBox margTop" id="coursebox" placeholder="Type in a Recipient" />
                    </div>
                    <div id="displays" class="disp"></div>
                    <div class="uiScrollable">
                        <ol id="update" class="timeline"></ol>
                    </div>
                
                    <div class="replySendBlock">
                        <div class="theBlock">
                            <div class="boxDir">
                                <input type="hidden" name="contentsender" id="contentsender" value="<?php echo $this->session->userdata('user_id'); ?>" />
                                <textarea name="content" id="content" class="textBoxMsg" placeholder="Write a message..."></textarea>
                            </div>
                        </div>
                        <div id="flash"></div>
                        <p align="right">
                        <input type="submit" name="submit" value="Send" class="update_button uibutton large" />
                        </p>
                    </div>
                </form>
            </div>

        </div>
        <?php } else { ?>
        <div class="col-md-5 noPad rightBorder" id="rightcolumn"></div>
        <?php } ?>
    </div>
</div>