<div class="col-md-12 noPad">
        <div class="actDialog">
            <div class="dialogHead">
                Send Message
            </div>
            <div id="notif"></div>
                <div class="uiLay">
                        <div class="control-group form-group ragaformarFix" style="margin-bottom: 15px !important;">
                          <div class="controls">
                              <input type="text" id="subject" name="subject" class="form-control ragacontrols textFormControls" placeholder="Subject">
                              <input type="hidden" id="to_rm_id" name="to_rm_id" value="<?php echo $this->uri->segment(3); ?>" class="form-control ragacontrols textFormControls">
                          </div>
                        </div>
                        <div class="control-group form-group ragaformarFix" style="margin-bottom: 15px !important;">
                          <div class="controls">
                              <textarea class="form-control" id="message" name="message" placeholder="Please enter your message here..." rows="5"></textarea>
                          </div>
                        </div>
                </div>
                <div class="uiOverlayFooter">
                    <input type="submit" name="submit" id="submit" value="Finish" class="btn btnfix greenbtn" style="margin-bottom: 3px; margin-top: 3px;" />
                </div>
</div>
</div>
<!--	<script src="<?php echo base_url('node_modules/socket.io/node_modules/socket.io-client/socket.io.js');?>"></script>
	<script>
  $(document).ready(function(){
    $("#submit").click(function(){
       var dataString = { 
              subject : $("#subject").val(),
              message : $("#message").val(),
              to_rm_id: $("#to_rm_id").val(),
            };
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('messaging/submitmsg');?>",
            data: dataString,
            dataType: "json",
            cache : false,
            success: function(data){
              $("#subject").val('');
              $("#message").val('');
              if(data.success == true){
                $("#notif").html(data.notif);
                var socket = io.connect( 'http://'+window.location.hostname+':3000' );
                socket.emit('new_count_message', { 
                  new_count_message: data.new_count_message
                });
                socket.emit('new_message', { 
                  subject: data.subject,
                  created_at: data.created_at,
                  id: data.email_tran_id
                });
              } else if(data.success == false){
                $("#subject").val(data.subject);
                $("#message").val(data.message);
                $("#notif").html(data.notif);
              }
          
            } ,error: function(xhr, status, error) {
              alert(error);
            },
        });
    });
  });
	</script>-->