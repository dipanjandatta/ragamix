<div class="container stanPad">
    <div class="row" style="margin-top: 20px;">
        <div class="col-md-12 noPad">
                <div class="col-md-12 listingPropaganda">
                    <div class="col-md-12 margBot fontMed headingPropaganda">
                        <div class="col-md-6 noPad">
                            Featured Ads
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="featuredads"></div>
                    </div>
                </div>
            <div class="col-md-3 noPad">
                <div class="col-md-12 noPad">
                    <div class="panel panel-default noBord">
                      <div class="panel-heading panelSubMain">
                       Wanted
                      </div>
                      <div class="panel-body panFan banban">
                          <div id="adswanted"></div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="col-md-12 listingPropaganda">
                    <div class="col-md-12 margBot fontMed headingPropaganda">
                        <div class="col-md-6 noPad">
                            Buy / Sell
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="adsbuysell"></div>
                    </div>
                </div>
                <div class="col-md-12 listingPropaganda">
                    <div class="col-md-12 margBot fontMed headingPropaganda">
                        <div class="col-md-6 noPad">
                            Offers
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="adsoffer"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>