<div class="container stanPad">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 stanPad">
            <div class="panel panel-default noBord">
                <div class="panel-heading panelLogin remwhitebord">
                    <div class="panel-body panCol">
                        <?php echo form_open('welcome/changepassword'); ?>
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls">
                                <input type="password" name="oldpass" class="form-control ragacontrols" placeholder="Old Password">
                            </div>
                        </div>
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls">
                                <input type="password" name="newpass" class="form-control ragacontrols" placeholder="New Password">
                            </div>
                        </div>
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls">
                                <input type="password" name="cnfpass" class="form-control ragacontrols" placeholder="Confirm New Password">
                            </div>
                        </div>
                        <div class="control-group form-group ragaformarFix">
                            <div class="controls col-sm-6 noPad" style="float: left; text-align: right;     margin: 5px 0px 3px 0px;">
                                <button type="submit" class="btn btnfix greenbtn" style="padding: 7px 45px !important;">CHANGE</button>
                            </div>
                            <div class="controls col-sm-6 noPad" style="float: right; text-align: right;     margin: 5px 0px 3px 0px;">
                                <button type="reset" class="btn btnfix greenbtn" style="padding: 7px 50px !important;" data-dismiss="modal">RESET</button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>