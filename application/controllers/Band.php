<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

class Band extends CI_Controller {
        function __construct(){
            parent::__construct(); // needed when adding a constructor to a controller
            $this->load->model('Profilemodel');
        } 
        
        function bsa(){
            if($this->uri->segment(3) != ''){
                $uriGot = $this->uri->segment(3);
            }
            else {
                $uriGot = $this->session->userdata('user_id');
            }
            $data['getAllAudios'] = $this->Profilemodel->getAudios($uriGot);
            @$this->db->free_db_resource();
            $data['getAllVideos'] = $this->Profilemodel->getVideos($uriGot);
            $this->load->view('app/bandsongsalbum', $data); 
        }
        
        function bpg(){
            if($this->uri->segment(3) != ''){
                $uriGot = $this->uri->segment(3);
            }
            else {
                $uriGot = $this->session->userdata('user_id');
            }
            $data['getPhotos'] = $this->Profilemodel->getProfilePhotos($uriGot);
            $this->load->view('app/bandgallery', $data);
        }
            
        function ba(){
            if($this->uri->segment(3) != ''){
                $uriGot = $this->uri->segment(3);
            }
            else {
                $uriGot = $this->session->userdata('user_id');
            }
            $data['previewData'] = $this->Profilemodel->getPreviewData($uriGot);
            $this->load->view('app/bandprofile', $data);
        }
        
        function bst(){
            if($this->uri->segment(3) != ''){
                $uriGot = $this->uri->segment(3);
            }
            else {
                $uriGot = $this->session->userdata('user_id');
            }
            $data['getEvents'] = $this->Profilemodel->getShowsEventsData($uriGot,$show_ev_id=0);
            $this->load->view('app/bandshows', $data);
        }
                  
    }
?>
