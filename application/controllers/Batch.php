<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
class Batch extends CI_Controller {
    
    function __construct() {
        parent::__construct();
            if(!$this->session->userdata('user_id')){
                $this->session->set_flashdata('messageError', 'Please LogIn to perform Operations');
                redirect($this->agent->referrer());
            }
            $this->load->model('Batchmodel');
            $this->load->model('Profilemodel');
    }
    
    function updatefb(){
        $data['recs'] = $this->Batchmodel->getFbLinkList($position=0,$item=1);
        $data['tp'] = $totPages = ceil($data['recs']['count'] / 100);
        $data['totalRecs'] = $data['recs']['count'];
        $this->load->view('admin/updatefb', $data);
    }
    
    function getupdatedFB(){
        $pos = $this->uri->segment(3);
        $recs = $this->uri->segment(4);
        $data['recs'] = $this->Batchmodel->getFbLinkList($pos,$recs);
        for($i=0; $i<sizeof($data['recs']['records']); $i++){
            $getData[$i] = $this->facebookLikeCount($data['recs']['records'][$i]->facebook_url);
            $tobeupdatedLc = $getData[$i][0]->like_count;
            @$this->db->free_db_resource();
            $this->Batchmodel->fbUpdateLikeCount($data['recs']['records'][$i]->basic_profile_id,$tobeupdatedLc);
            
        }
        redirect('batch/updatefb');
    }
    
    function updateyoutube(){
        $data['recs'] = $this->Batchmodel->getYoutubeList($position=0,$item=1);
        $data['tp'] = $totPages = ceil($data['recs']['count'] / 100);
        $data['totalRecs'] = $data['recs']['count'];
        $this->load->view('admin/updateyoutube', $data);
    }
    
    function facebookLikeCount($url){
        $fql  = "SELECT share_count, like_count, comment_count ";
        $fql .= " FROM link_stat WHERE url = '$url'";
        $fqlURL = "https://api.facebook.com/method/fql.query?format=json&query=" . urlencode($fql);
        $response = file_get_contents($fqlURL);
        return json_decode($response);
    }
    
    function getupdatedyoutube(){
        $pos = $this->uri->segment(3);
        $recs = $this->uri->segment(4);
        $data['recs'] = $this->Batchmodel->getYoutubeList($pos,$recs);
        for($i=0; $i<sizeof($data['recs']['records']); $i++){
            $getData[$i] = $this->youTubeCount($data['recs']['records'][$i]->youtube_url);
            $tobeupdatedLc = $getData[$i]->items[0]->statistics->viewCount;
            @$this->db->free_db_resource();
            $this->Batchmodel->youtubeUpdateViewCount($data['recs']['records'][$i]->serial_no,$tobeupdatedLc);
            
        }
        redirect('batch/updateyoutube');
      
    }
    
    function youTubeCount($youtubeurl){
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $youtubeurl, $match)) {
            $vid = $match[1];
        }                            
        $video_ID = $vid;
        $JSON = file_get_contents("https://www.googleapis.com/youtube/v3/videos?id=".$video_ID."&key=AIzaSyBhEsWdKigo4OsNCKBR7ucq3Bh0BvtmaVE&part=snippet,statistics");
        $JSON_Data = json_decode($JSON);
        return $JSON_Data;
    }
    
    function featuredProfiles(){ 
            if($this->uri->segment(3) == 'HomePage'){
                $data['fps'] = $this->Batchmodel->getFeaturedProfileList();
            }
            else if($this->uri->segment(3) == 'Musician'){
                $cat = 'Musician';
                $data['fps'] = $this->Profilemodel->getFeaturedMusician($cat);
            }
            else if($this->uri->segment(3) == 'Singer'){
                $cat = 'Singer';
                $data['fps'] = $this->Profilemodel->getFeaturedMusician($cat);
            }
            else if($this->uri->segment(3) == 'Band'){
                $cat = 'Band';
                $data['fps'] = $this->Profilemodel->getFeaturedMusician($cat);
            }
            @$this->db->free_db_resource();
            if($this->uri->segment(3) == 'HomePage' && $this->uri->segment(4) == '1'){
                $uname = $this->uri->segment(5);
                $data['users'] = $this->Batchmodel->get_profiles_for_featured($category='',$uname);
            }
            else if($this->uri->segment(3) == 'Musician' && $this->uri->segment(4) == '1') {
                $cater = 'Musician';
                $uname = $this->uri->segment(5);
                $data['users'] = $this->Batchmodel->get_profiles_for_featured($cater,$uname);
            }
            else if($this->uri->segment(3) == 'Band' && $this->uri->segment(4) == '1') {
                $cater = 'Band';
                $uname = $this->uri->segment(5);
                $data['users'] = $this->Batchmodel->get_profiles_for_featured($cater,$uname);
            }
            else if($this->uri->segment(3) == 'Singer' && $this->uri->segment(4) == '1') {
                $cater = 'Singer';
                $uname = $this->uri->segment(5);
                $data['users'] = $this->Batchmodel->get_profiles_for_featured($cater,$uname);
            }
            
        
        $this->load->view('admin/featuredprofiles', $data);
    }
    
    function featuredProfilesList(){
        $selval = $this->input->post('fprofs');
        redirect('batch/featuredprofiles/'.$selval);
    }
    
    function featuredProfilesSearch(){
        $segVal = $this->input->post('fp');
        if($this->input->post('uname') == ''){
            redirect('batch/featuredprofiles/'.$segVal.'/1');
        }
        else {
            redirect('batch/featuredprofiles/'.$segVal.'/1/'.$this->input->post('uname'));
        }
    }
    
    function postPosition(){
        @$this->db->free_db_resource();
        if($this->input->post('sv') == 'HomePage'){
            $data = array(
                        'user_name_in' => $this->input->post('uname'),
                        'rm_id_in' => $this->input->post('rmid'),
                        'position_in' => $this->input->post('positionvector')
                );
            $statmsg = $this->Batchmodel->AddFeaturedProfile($data);
        }
        else {
            $dataOther = array(
                        'display_page_in' => $this->input->post('sv'),
                        'rm_id_in' => $this->input->post('rmid'),
                        'position_in' => $this->input->post('positionvector')
                );
            $statmsg = $this->Batchmodel->AddFeaturedProfileOthers($dataOther);
        }
        $this->session->set_flashdata('messageError', $statmsg);
        redirect('batch/featuredProfiles/'.$this->input->post('sv'));
    }
    
    function deleteFeaturedProfile(){
        $fid = $this->uri->segment(4);
        if($this->uri->segment(3) == 'HomePage') {
            $this->Batchmodel->deleteFProfile($fid);
        } else {
            $this->Batchmodel->deleteFProfileOthers($fid);
        }
        redirect('batch/featuredprofiles/'.$this->uri->segment(3));
    }
    
    
    
}
?>