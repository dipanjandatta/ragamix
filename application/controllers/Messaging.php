<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
class Messaging extends CI_Controller {

        function send(){
            $this->load->view('app/send');
        }
        
        function submitmsg(){
		$arr['subject_line'] = $this->input->post('subject');
		$arr['body_text'] = $this->input->post('message');
                $arr['to_rm_id'] = $this->input->post('to_rm_id');
                $arr['from_rm_id'] = $this->session->userdata('user_id');
			$this->db->insert('mailbox',$arr);
			$detail = $this->db->select('*')->from('mailbox')->where('email_tran_id',$this->db->insert_id())->get()->row();
			$arr['subject'] = $detail->subject;
			$arr['created_at'] = $detail->created_at;
			$arr['id'] = $detail->email_tran_id;
			$arr['new_count_message'] = $this->db->where('has_read',0)->count_all_results('mailbox');
			$arr['success'] = true;
			$arr['notif'] = '<div class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 alert alert-success" role="alert"> <i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Message sent ...</div>';
		
		echo json_encode($arr);
        }
        
        function messages(){
                $this->load->model('Postmodel');
		$data['message'] = $this->db->select('*')->from('mailbox')->order_by('email_tran_id','desc')->get();
                $data['msg_cnt'] = $this->Postmodel->getMsgCnt($this->session->userdata('user_id'));
		$this->load->view('app/messages',$data);
        }
        
        	public function detail(){

		$detail = $this->db->select('*')->from('mailbox')->where('email_tran_id',$this->input->post('id'))->get()->row();

		if($detail){

			$this->db->where('email_tran_id',$this->input->post('id'))->update('mailbox',array('has_read'=>1));
			$arr['subject'] = $detail->subject_line;
			$arr['message'] = $detail->body_text;
			$arr['created_at'] = $detail->created_at;
			$arr['update_count_message'] = $this->db->where('read_status',0)->count_all_results('mailbox');
			$arr['success'] = true;

		} else {

			$arr['success'] = false;
		}

		
		
		echo json_encode($arr);

	}
                    
    }
?>
