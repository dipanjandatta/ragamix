<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
class Seek extends CI_Controller {
    var $data;

    function __construct(){
        parent::__construct(); // needed when adding a constructor to a controller
        $this->data = array(
            'layoutmode' => $this->config->item('layoutconfigdev')
        );
        $this->load->model('Profilemodel');
    }
    function view(){

        $body_data['next'] = $next = $_GET['next'];
        $body_data['clicks'] = $clicks = $_GET['clicks'];
        $body_data['page_number'] = $page_number = $_GET['page_number'];
        $body_data['prev'] = $prev = $_GET['prev'];

        if($prev == 'y'){
            $data['next'] = $next = $next+1;
            $body_data['clicks'] = $clicks = $clicks-1;
            $body_data['page_number'] = $page_number = $clicks;
        }
        $position = ($page_number * 20);
        if($page_number != 0) {

        }
        else{
            $page_number = 0;
        }
        $data['next'] = $next = $next+1;
        $body_data['clicks'] = $clicks = $clicks+1;
        $body_data['page_number'] = $page_number = $clicks;

        $body_data['seek'] = $this->Profilemodel->getSeekingRequirements(0,$chckstat=1,$position,$item=20);
//        print_r($body_data['seek']);exit();
        $body_data['seeklist']=$body_data['seek']['records'];
        $body_data['mc']=$body_data['seek']['count'];
//        print_r($body_data['seeklist']);exit();

        $layout_data['pageTitle'] = "RAGAMIX - Jobs & Opportunities ";
        $layout_data['meta_description'] = "Jobs Opportunities";
        $layout_data['meta_keywords'] = "Jobs Opportunities";
        $layout_data['meta_url'] = "$base_url";
        $layout_data['image'] = "".base_url()."images/ragamixnewlogo.jpg";

        $layout_data['content_body'] = $this->load->view('app/seek/seekview', $body_data, true);

        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    /*Added By Souvick*/

    function seekdetails(){

        $this->data = array(
            'layoutmode' => $this->config->item('layoutconfigdev')
        );

        $data['seek'] = $this->Profilemodel->getSeekingRequirements(0,1,0,$item=3);
        @$this->db->free_db_resource();

        $data['seeklist']=$data['seek']['records'];

//        print_r($data['seeklist']);exit();

        $query = $this->db->query("select a.*, b.user_name from seeking a, users b where a.serial_no = ? and a.rm_id = b.rm_id", array('serial_no'=>$this->uri->segment(3)));
        $result = $query->result();

        $query1 = $this->db->query("select a.pic_url,a.photo_id from photos a, profile_photos b where b.rm_id = ? and main_pic=1 and a.photo_id = b.photo_id", array('rm_id'=>@$result[0]->rm_id));
        $result1 = $query1->result();

        $returnValue = str_replace(' ', '%20', @$result1[0]->pic_url);

//        print_r($result1);exit();
        $data['pic_url'] = $returnValue;
        $data['sid'] = @$result[0]->serial_no;
        $data['rmid'] = @$result[0]->rm_id;
        $data['keyword'] = @$result[0]->keyword;
        $data['location'] = @$result[0]->location;
        $data['description'] = @$result[0]->description;
        $data['hashtags'] = @$result[0]->hashtags;
        $data['updated_on'] = date('Y-m-d', strtotime(@$result[0]->updated_on));
        $data['user_name'] = @$result[0]->user_name;
//		 $layout_data['pageTitle'] = "Ragamix Opportunities";
        $layout_data['pageTitle'] = @$result[0]->keyword;
        $layout_data['meta_description'] = substr(@$data['description'], 0, 500)."...";
        $layout_data['meta_keywords'] = $data['keyword']."".substr($data['description'], 0, 500);
        $layout_data['meta_url'] = "";
        $layout_data['meta_classification'] = "Opportunities";
        $layout_data['image'] = base_url()."images/ragamixnewlogo.jpg";
        $data['js_to_load'] = '';
        $layout_data['content_body'] = $this->load->view('ajaxcontent/seekdetails', $data, true);

        $this->load->view($this->data['layoutmode'], $layout_data);
    }

    function sendmessagefromseek(){
        $toData = $this->input->post("toid");
        $fromData = $this->session->userdata('user_id');
        $contentData = $this->input->post("body_text");
        $data = array(
            'from_rm_id' => $fromData,
            'to_rm_id' => $toData,
            'subject_line' => '',
            'body_text' => $contentData,
            'view_type' => 'Apply',
            'mail_direction' => 0,
            'mail_timestamp' => date('Y-m-d H:i:s'),
            'has_attachment' => 0,
            'has_read' => 0,
            'from_email_id' => '',
            'to_email_id' => '',
            'from_user' => '',
            'to_user' => ''
        );

//        print_r($data);exit();
        $this->Profilemodel->postMessage($data);
        $this->session->set_flashdata('messageError', 'Message Sent Successfully');
        redirect($this->agent->referrer());
    }

}
?>
