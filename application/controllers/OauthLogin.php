<?php defined('BASEPATH') OR exit('No direct script access allowed');
class OauthLogin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
                $fb_config = array(
                                    'appId'  => '1162483030462581',
                                    'secret' => '0b2601dac0da430039f8c6caad4fa16f'
                        );

                $this->load->library('facebook', $fb_config);
                $this->load->model('Postmodel');
	}

        
        function fblogin(){
                $dta['test'] = $this->facebook->getLoginUrl(array('redirect_uri' => site_url('oauthLogin/flogin'),'scope' => array("email")));
                redirect($dta['test']);
        }
        

	public function flogin()
	{
	    $user = "";
	    $userId = $this->facebook->getUser();
        if ($userId) {
            try {
                $user = $this->facebook->api('/me?fields=name,email');
            } catch (FacebookApiException $e) {
                $user = "";
            }
        }
        else {
            echo 'Please try again.'; exit;
        }
        if($user!=""){
            //print_r($user);
                $dataFB = array(
                                'user_name_in' => $user['id'],
                                'password_in' => '',
                                'fb_login' => 1
                    );
                $data['retVal'] = $this->Postmodel->postLoginDetails($dataFB);
                //print_r($data['retVal']); exit();
                if($data['retVal'][0]->rm_out_param > 0 && $data['retVal'][0]->active_out_param == 1){
                    //Assign Session Variables
                    $this->session->set_userdata(
                                                    array(
                                                            'user_id' => $data['retVal'][0]->rm_out_param,
                                                            'user_name' => $data['retVal'][0]->user_name_param,
                                                    )
                                            );


                    redirect($this->agent->referrer());
                }
                else {
                    redirect('welcome/index/roadBlock/'.base64_encode($user['email']).'/'.base64_encode($user['id']).'/XyZ');
                }
                
            
        }
        else{
                $dta['test'] = $this->facebook->getLoginUrl(array('redirect_uri' => site_url('oauthLogin/flogin'),'scope' => array("email")));
                redirect($dta['test']);
        }
        
    }
    

    function roadblockcomplete(){
                //$fbVal = base64_decode($this->uri->segment(5));
                $actKey = uniqid();
                $data = array(
                                'user_cat_in' => '',
                                'user_name_in' => trim($this->input->post('user_name_in')),
                                'facebook_id_in' => base64_decode($this->input->post('facebook_id_in')),
                                'password_in' => '',
                                'email_in' => base64_decode($this->input->post('email_in')),
                                'mobile_no_in' => '',
                                'city_name_in' => '',
                                'country_name_in' => 'India',
                                'view_count' => 0,
                                'user_type_in' => 1,
                                'signup_date' => date('Y-m-d H:i:s'),
                                'updated_on' => date('Y-m-d H:i:s'),
                                'activation_key_in' => $actKey
                );
                $rmIdVal = $this->Postmodel->getSignUp($data);
                if($rmIdVal > 0){
                    //Send Activation Mail and redirect
                    $item = $this->config->item('app_master_url');
                    //Creating the json object as desired to pass in activation mail API
                    $desiredJson = array(
                                            'email_id' => base64_decode($this->input->post('email_in')),
                                            'user_name' => trim($this->input->post('user_name_in')),
                                            'userKey' => $actKey
                                    );
                    $json = json_encode($desiredJson);
                    $retval = $this->Postmodel->sendEmailActivation($json,$item);
                    
                    $this->session->set_flashdata('messageError', 'An activation link has been sent to your mail ID.');
                    redirect('welcome');
                }
                else {
                    $this->session->set_flashdata('messageError', 'Ragamix ID or Username already exists..Choose another and sign up');
                    redirect('welcome/index/roadBlock/'.base64_encode($this->input->post('email_in').'/'.base64_encode($this->input->post('facebook_id_in'))));
                }
                
    }
}